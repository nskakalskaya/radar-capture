#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    qRegisterMetaType < QVector<double> > ("QVector<double>");
    qRegisterMetaType < QVector<float> > ("QVector<float>");
    qRegisterMetaType <int16_t> ("int16_t");

    cfgMain = new ConfigBase("general.ini");

    rotator = new Rotator(cfgMain);
    rotator->moveToThread(&rotatorThread);

    settings = new SettingsWidget(cfgMain, this);
    settings->setWindowTitle("Configuration");
    settings->hide();

    createActions();
    createMenus();


// -------------------------------------- Connect Group -----------------------------------------

    rotConnectButton = new QPushButton("Connect", this);
    rotConnectButton->setFixedWidth(70);

    QHBoxLayout *connectRotatorLayout = new QHBoxLayout;
    connectRotatorLayout->addWidget(rotConnectButton);

    connectGroupBox = new QGroupBox(this);
    connectGroupBox->setLayout(connectRotatorLayout);
    connectGroupBox->setFixedWidth(100);
    connectGroupBox->setFixedHeight(50);


// ------------------------------- Current Position Group -----------------------------------

    currentPanLabel = new QLabel(this);
    currentPanLabel->setFixedWidth(100);
    currentPanLabel->setText("Pan:");
    currentTiltLabel = new QLabel(this);
    currentTiltLabel->setFixedWidth(100);
    currentTiltLabel->setText("Tilt:");

    QHBoxLayout *currentPositionLayout = new QHBoxLayout;
    currentPositionLayout->addWidget(currentPanLabel);
    currentPositionLayout->addWidget(currentTiltLabel);

    currentPositionGroupBox = new QGroupBox("Rotator Position", this);
    currentPositionGroupBox->setLayout(currentPositionLayout);
    currentPositionGroupBox->setFixedWidth(170);
    currentPositionGroupBox->setFixedHeight(50);


// ----------------------------------- Movement Group -------------------------------------

    rotLeftButton = new QPushButton("Left", this);
    rotLeftButton->setFixedWidth(70);
    rotLeftButton->setAutoRepeat(true);
    rotRightButton = new QPushButton("Right", this);
    rotRightButton->setFixedWidth(70);
    rotRightButton->setAutoRepeat(true);
    rotUpButton = new QPushButton("Up", this);
    rotUpButton->setFixedWidth(70);
    rotUpButton->setAutoRepeat(true);
    rotDownButton = new QPushButton("Down", this);
    rotDownButton->setFixedWidth(70);
    rotDownButton->setAutoRepeat(true);

    QGridLayout *movementLayout = new QGridLayout;
    movementLayout->addWidget(rotUpButton, 0, 1);
    movementLayout->addWidget(rotDownButton, 1, 1);
    movementLayout->addWidget(rotLeftButton, 1, 0);
    movementLayout->addWidget(rotRightButton, 1, 2);

    movementGroupBox = new QGroupBox("Rotator Movement", this);
    movementGroupBox->setLayout(movementLayout);
    movementGroupBox->setFixedWidth(250);
    movementGroupBox->setFixedHeight(110);


// -------------------------------- Set Position Group ---------------------------------------

    setPanEdit = new QLineEdit(this);
    setPanEdit->setFixedWidth(60);
    setPanEdit->setText("0.00");
    setTiltEdit = new QLineEdit(this);
    setTiltEdit->setFixedWidth(60);
    setTiltEdit->setText("0.00");
    setPositionButton = new QPushButton("Set", this);
    setPositionButton->setFixedWidth(70);


    QHBoxLayout *setPosLabelsLayout = new QHBoxLayout;
    setPosLabelsLayout->addWidget(new QLabel("Pan", this), 0, Qt::AlignHCenter);
    setPosLabelsLayout->addWidget(new QLabel("Tilt", this), 0, Qt::AlignHCenter);

    QHBoxLayout *setPosEditsLayout = new QHBoxLayout;
    setPosEditsLayout->addWidget(setPanEdit);
    setPosEditsLayout->addWidget(setTiltEdit);

    QVBoxLayout *setPositionLayout = new QVBoxLayout;
    setPositionLayout->addLayout(setPosLabelsLayout);
    setPositionLayout->addLayout(setPosEditsLayout);
    setPositionLayout->addWidget(setPositionButton, 0, Qt::AlignCenter);

    setPositionGroupBox = new QGroupBox("Set Position", this);
    setPositionGroupBox->setLayout(setPositionLayout);
    setPositionGroupBox->setFixedWidth(170);
    setPositionGroupBox->setFixedHeight(110);


// ------------------------------------------- Scan Group ----------------------------------------------

    scanButton = new QPushButton(this);
    scanButton->setFixedWidth(70);
    scanButton->setText("Start");

    QVBoxLayout *scanLayout = new QVBoxLayout;
    scanLayout->addWidget(scanButton, 0, Qt::AlignHCenter);

    scanGroupBox = new QGroupBox("Scan", this);
    scanGroupBox->setLayout(scanLayout);
    scanGroupBox->setFixedWidth(150);
    scanGroupBox->setFixedHeight(110);


// ------------------------------------- Scan Settings Group --------------------------------------------


    setScanPanBeginEdit = new QLineEdit(this);
    setScanPanBeginEdit->setFixedWidth(70);
    setScanPanBeginEdit->setText("-1.00");
    setScanPanEndEdit = new QLineEdit(this);
    setScanPanEndEdit->setFixedWidth(70);
    setScanPanEndEdit->setText("1.00");
    setScanPanStepEdit = new QLineEdit(this);
    setScanPanStepEdit->setFixedWidth(70);
    setScanPanStepEdit->setText("1.00");
    setScanTiltBeginEdit = new QLineEdit(this);
    setScanTiltBeginEdit->setFixedWidth(70);
    setScanTiltBeginEdit->setText("0.00");
    setScanTiltEndEdit = new QLineEdit(this);
    setScanTiltEndEdit->setFixedWidth(70);
    setScanTiltEndEdit->setText("2.00");
    setScanTiltStepEdit = new QLineEdit(this);
    setScanTiltStepEdit->setFixedWidth(70);
    setScanTiltStepEdit->setText("2.00");


    QGridLayout *scanSettingsLayout = new QGridLayout;

    scanSettingsLayout->addWidget(new QLabel("Pan:", this), 1, 0, Qt::AlignRight);
    scanSettingsLayout->addWidget(new QLabel("Tilt:", this), 2, 0, Qt::AlignRight);

    scanSettingsLayout->addWidget(new QLabel("Begin:", this), 0, 1, Qt::AlignHCenter);
    scanSettingsLayout->addWidget(setScanPanBeginEdit, 1, 1);
    scanSettingsLayout->addWidget(setScanTiltBeginEdit, 2, 1);

    scanSettingsLayout->addWidget(new QLabel("End:", this), 0, 2, Qt::AlignHCenter);
    scanSettingsLayout->addWidget(setScanPanEndEdit, 1, 2);
    scanSettingsLayout->addWidget(setScanTiltEndEdit, 2, 2);

    scanSettingsLayout->addWidget(new QLabel("Step:", this), 0, 3, Qt::AlignHCenter);
    scanSettingsLayout->addWidget(setScanPanStepEdit, 1, 3);
    scanSettingsLayout->addWidget(setScanTiltStepEdit, 2, 3);

    scanSettingsGroupBox = new QGroupBox("Scan Settings", this);
    scanSettingsGroupBox->setLayout(scanSettingsLayout);
    scanSettingsGroupBox->setFixedWidth(270);
    scanSettingsGroupBox->setFixedHeight(110);


// ------------------------------- Combining Into hLayouts ------------------------------------

    QHBoxLayout *firstHLayout = new QHBoxLayout;
    firstHLayout->addWidget(connectGroupBox);
    firstHLayout->addStretch(1);
    firstHLayout->addWidget(currentPositionGroupBox);

    QHBoxLayout *secondHLayout = new QHBoxLayout;
    secondHLayout->addStretch(10);
    secondHLayout->addWidget(movementGroupBox);
    secondHLayout->addStretch(1);
    secondHLayout->addWidget(setPositionGroupBox);

    QHBoxLayout *thirdHLayout = new QHBoxLayout;
    thirdHLayout->addWidget(scanGroupBox);
    thirdHLayout->addWidget(scanSettingsGroupBox);



    // All on single vLayout
    QVBoxLayout *guiLayout = new QVBoxLayout;
    guiLayout->addLayout(firstHLayout);
    guiLayout->addLayout(secondHLayout);
    guiLayout->addLayout(thirdHLayout);

    // Placing on frame to fix size
    QFrame *mainFrame = new QFrame;
    mainFrame->setLayout(guiLayout);

    QHBoxLayout *mainHLayout = new QHBoxLayout;
    mainHLayout->addWidget(mainFrame, 0, Qt::AlignHCenter);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(mainHLayout);

    QWidget *mainWidget = new QWidget;
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);

    setGroupsEnabled(GRMASK_CONNECT | GRMASK_SETTINGS);
    rotatorIsConnected = false;
    scanIsRunning = false;
    scanIsLooped = false;
    scanResume = false;

    statusBarLabel = new QLabel("", this);
    QFont statusBarFont("Times", 9);
    statusBarLabel->setFont(statusBarFont);
    statusBar()->addPermanentWidget(statusBarLabel, 1);

// --------------------------- Signal/Slot Connections --------------------------------------------

    // Settings
    connect(settings, &SettingsWidget::updateSettings, this, &MainWindow::updateSettings);
    connect(settings, &SettingsWidget::updateSettings, rotator, &Rotator::updateSettings);

    // Connect
    connect(rotConnectButton, &QPushButton::pressed, this, &MainWindow::connectRotator);

    // Current position
    connect(rotator, &Rotator::sendRotPan, currentPanLabel, &QLabel::setText);
    connect(rotator, &Rotator::sendRotTilt, currentTiltLabel, &QLabel::setText);

    // Movement
    connect(rotLeftButton, &QPushButton::pressed, rotator, &Rotator::left);
    connect(rotRightButton, &QPushButton::pressed, rotator, &Rotator::right);
    connect(rotUpButton, &QPushButton::pressed, rotator, &Rotator::up);
    connect(rotDownButton, &QPushButton::pressed, rotator, &Rotator::down);
    connect(rotLeftButton, &QPushButton::released, rotator, &Rotator::stop);
    connect(rotRightButton, &QPushButton::released, rotator, &Rotator::stop);
    connect(rotUpButton, &QPushButton::released, rotator, &Rotator::stop);
    connect(rotDownButton, &QPushButton::released, rotator, &Rotator::stop);

    // Set position
    connect(setPositionButton, &QPushButton::pressed, this, &MainWindow::setPositionOfRotator);

    // Scan
    connect(scanButton, &QPushButton::pressed, this, &MainWindow::startScan);
    connect(this, &MainWindow::startScanSignal, rotator, &Rotator::startScan);
    connect(rotator, &Rotator::stoppingScan, this, &MainWindow::stopScanFromRotator);

    // Radar
    connect(&rotatorThread, &QThread::started, rotator, &Rotator::passProcess);
    connect(this, &MainWindow::sendConnectRotator, rotator, &Rotator::connectToRotator);
    connect(this, &MainWindow::sendDisconnectRotator, rotator, &Rotator::disconnectFromRotator);
    connect(rotator, &Rotator::rotatorConnected, this, &MainWindow::rotatorConnected);
    connect(rotator, &Rotator::rotatorDisconnected, this, &MainWindow::rotatorDisconnected);

    updateSettings();

    rotatorThread.start();
}

void MainWindow::setGroupsEnabled(uint8_t flags)
{
    connectGroupBox->setEnabled(flags & GRMASK_CONNECT);
    currentPositionGroupBox->setEnabled(flags & GRMASK_CUR_POS);
    movementGroupBox->setEnabled(flags & GRMASK_MOVE);
    setPositionGroupBox->setEnabled(flags & GRMASK_SET_POS);
    scanGroupBox->setEnabled(flags & GRMASK_SCAN);
    scanSettingsGroupBox->setEnabled(flags & GRMASK_SCAN_SETT);
    settingsConfAct->setEnabled(flags & GRMASK_SETTINGS);
}

void MainWindow::createActions()
{
    // File actions
    fileQuitAct = new QAction("Quit", this);
    fileQuitAct->setShortcut(Qt::Key_Escape);
    connect(fileQuitAct, &QAction::triggered, this, &MainWindow::close);

    // Settings actions
    settingsConfAct = new QAction("Configure", this);
    settingsConfAct->setShortcut(Qt::Key_F12);
    connect(settingsConfAct, &QAction::triggered, settings, &SettingsWidget::show);
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu("File");
    fileMenu->addAction(fileQuitAct);

    settingsMenu = menuBar()->addMenu("Settings");
    settingsMenu->addAction(settingsConfAct);
}

void MainWindow::updateSettings()
{
    setScanLoop(cfgMain->getScanLoop());
}

void MainWindow::startScan()
{
    if (!scanIsRunning)
    {
        int16_t panBegin = setScanPanBeginEdit->text().toFloat() * 100;
        int16_t panEnd = setScanPanEndEdit->text().toFloat() * 100;
        int16_t panStep = setScanPanStepEdit->text().toFloat() * 100;

        int16_t tiltBegin = setScanTiltBeginEdit->text().toFloat() * 100;
        int16_t tiltEnd = setScanTiltEndEdit->text().toFloat() * 100;
        int16_t tiltStep = setScanTiltStepEdit->text().toFloat() * 100;

        if (panBegin >= panEnd || tiltBegin >= tiltEnd)
        {
            errorMessageBox("Begin must be less than End");
            return;
        }

        if ((panStep <= 0 || tiltStep <= 0))
        {
            errorMessageBox("Step must be greater than zero");
            return;
        }

        setGroupsEnabled(GRMASK_SCAN | GRMASK_CUR_POS);

        emit startScanSignal(panBegin, panEnd, panStep, tiltBegin, tiltEnd, tiltStep);

        scanIsRunning = true;
        scanButton->setText("Stop");
        statusBarLabel->setText("Scan is running");

        disconnect(scanButton, &QPushButton::pressed, this, &MainWindow::startScan);
        connect(scanButton, &QPushButton::pressed, this, &MainWindow::terminateScan);
    }
}

void MainWindow::stopScanFromRotator()
{
    if (!scanIsLooped)
    {
        scanIsRunning = false;
        setGroupsEnabled(GRMASK_ALL);
        scanButton->setText("Start");
        statusBarLabel->setText("Ready");

        disconnect(scanButton, &QPushButton::pressed, this, &MainWindow::terminateScan);
        connect(scanButton, &QPushButton::pressed, this, &MainWindow::startScan);
    }
    else
    {
        scanIsRunning = false;
        startScan();
    }
}

void MainWindow::terminateScan()
{
    rotator->stopScanFromMain();

    scanIsRunning = false;
    setGroupsEnabled(GRMASK_ALL);
    scanButton->setText("Start");
    statusBarLabel->setText("Ready");

    disconnect(scanButton, &QPushButton::pressed, this, &MainWindow::terminateScan);
    connect(scanButton, &QPushButton::pressed, this, &MainWindow::startScan);
}

void MainWindow::setScanLoop(bool isLooped)
{
    scanIsLooped = isLooped;
}

MainWindow::~MainWindow()
{
    delete cfgMain;

    rotator->stopScanFromMain();
    emit sendDisconnectRotator();

    rotatorThread.quit();
    rotatorThread.wait();

    rotator->deleteLater();
}

void MainWindow::rotatorConnected(bool success)
{
    if (success)
    {
        rotatorIsConnected = true;
        setGroupsEnabled(GRMASK_ALL);
        rotConnectButton->setText("Disconnect");
        statusBarLabel->setText("Ready");
    }
    else
    {
        errorMessageBox("Failed to connect to rotator");
        return;
    }
}

void MainWindow::rotatorDisconnected(bool success)
{
    if (success)
    {
        rotatorIsConnected = false;
        setGroupsEnabled(GRMASK_CONNECT | GRMASK_SETTINGS);
        rotConnectButton->setText("Connect");
        currentPanLabel->setText("Pan:");
        currentTiltLabel->setText("Tilt:");
        statusBarLabel->setText("");
    }
    else
        errorMessageBox("Failed to disconnect from rotator");
}

void MainWindow::connectRotator()
{
    if (!rotatorIsConnected)
    {
        rotConnectButton->setText("Connecting...");
        emit sendConnectRotator();
    }
    else
    {
        rotator->stopScanFromMain();
        stopScanFromRotator();

        emit sendDisconnectRotator();
    }
}

void MainWindow::setPositionOfRotator()
{
    int16_t pan;
    int16_t tilt;
    pan = setPanEdit->text().toFloat() * 100;
    tilt = setTiltEdit->text().toFloat() * 100;

    rotator->writeRotPosition(pan, tilt);
}

void MainWindow::errorMessageBox(QString str)
{
    QMessageBox::information(this, "Error", str);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (!scanIsRunning)
        switch (event->key())
        {

        case Qt::Key_A:          
            rotator->left();
            break;

        case Qt::Key_D:
            rotator->right();
            break;

        case Qt::Key_W:
            rotator->up();
            break;

        case Qt::Key_S:
            rotator->down();
            break;

        }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (!event->isAutoRepeat() && !scanIsRunning)
        switch (event->key())
        {

        case Qt::Key_A:
        case Qt::Key_D:
        case Qt::Key_W:
        case Qt::Key_S:
            rotator->stop();
            break;

        }
}
