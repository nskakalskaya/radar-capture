#include "rotator.h"


Rotator::Rotator(ConfigBase *cfg, QObject *parent) :
    QObject(parent), cfgPtr(cfg)
{
    connect(this, &Rotator::passProcess, this, &Rotator::createUDPSocket);

    scanPanBegin = 0;
    scanPanEnd = 0;
    scanTiltBegin = 0;
    scanTiltEnd = 0;
    scanIsRunning = false;
    rotatorIsConnected = false;
    scanStarted = false;
    scanPanCur = 0;
    scanTiltCur = 0;
    scanPanBegin = 0;
    scanPanEnd = 0;
    scanPanStep = 0;
    scanTiltBegin = 0;
    scanTiltEnd = 0;
    scanTiltStep = 0;
    rotCommand = NONE;

    scanByStep = true;
    rotateByStep = true;

    rotPanStep = 3;
    rotTiltStep = 3;

    updateSettings();
}

Rotator::~Rotator()
{
    cfgPtr = NULL;
    if (rotatorIsConnected)
        disconnectFromRotator();

    delete udpSocket;
}

void Rotator::updateSettings()
{
    QString ipAddress = cfgPtr->getRotIPAddress();
    rotIPAddress.setAddress(ipAddress);
    rotPort = cfgPtr->getRotPort();
    rotAddr = cfgPtr->getRotAddr();
    speedPan = cfgPtr->getSpeedPan();
    speedTilt = cfgPtr->getSpeedTilt();
    zeroPanOffset = cfgPtr->getZeroPanOffset();
    zeroTiltOffset = cfgPtr->getZeroTiltOffset();
    panLimit = cfgPtr->getPanLimit();
    tiltLimit = cfgPtr->getTiltLimit();
    rotateByStep = cfgPtr->getRotateByStep();
    scanByStep = cfgPtr->getScanByStep();
    rotPanStep = cfgPtr->getRotatePanStep();
    rotTiltStep = cfgPtr->getRotateTiltStep();
}

void Rotator::createUDPSocket()
{
    udpSocket = new QUdpSocket(this);
    readPosTimer = new QTimer(this);
    connect(readPosTimer, &QTimer::timeout, this, &Rotator::readRotPosition);
}

void Rotator::setSpeedPan(uint8_t speed)
{
    speedPan = speed;
}

void Rotator::setSpeedTilt(uint8_t speed)
{
    speedTilt = speed;
}

void Rotator::connectToRotator()
{   
    if (!udpSocket->isValid())
    {
        if (udpSocket->bind(QHostAddress::Any, rotPort))
        {
//            rotatorIsConnected = true;
            readPosTimer->start(ROT_READ_TIMEOUT);
//            emit rotatorConnected(true);
        }
        else
        {
            udpSocket->close();
            emit rotatorConnected(false);
        }
    }
}

void Rotator::disconnectFromRotator()
{
    sendCommandToRotator(ROT_STOP);

    rotatorIsConnected = false;
    if (udpSocket->isValid())
    {
        readPosTimer->stop();
        udpSocket->close();
        emit rotatorDisconnected(true);
    }
    else
    {
        emit rotatorDisconnected(false);
    }
}

uint8_t Rotator::sendCommandToRotator(const uint8_t command, uint8_t param1, uint8_t param2)
{
    uint8_t checksum = rotAddr + command + param1 + param2;
    const char cmdStr[] = {(char)0xFF, (char)rotAddr, (char)0x00, (char)command, (char)param1, (char)param2, (char)checksum};

    if (udpSocket->isValid())
    {
        QByteArray data;
        data = data.fromRawData(cmdStr, 7);
        udpSocket->writeDatagram(data, rotIPAddress, rotPort);
        udpSocket->flush();
        udpSocket->waitForBytesWritten(500);
    }

    return 7;
}

void Rotator::readResponseFromRotator()
{   
    udpSocket->waitForReadyRead(500);

    while (udpSocket->hasPendingDatagrams())
    {
        rotDatagram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(rotDatagram.data(), rotDatagram.size());
        QByteArray tmp = rotDatagram.toHex();

        if (tmp.left(4) == "ff01")
        {
            if (!rotatorIsConnected)
            {
                rotatorIsConnected = true;
                emit rotatorConnected(true);
            }

            uint8_t rotResponse = tmp.mid(6, 2).toInt(0, 16);

            switch (rotResponse)
            {

            // Command completion response
            case ROT_RES_CMD:
                scan();
                break;

            // Pan response
            case ROT_RES_PAN:
                responseRotPan((uint16_t)roundf((float)tmp.mid(8, 4).toInt(0, 16) / ROT_RATIO_PAN));
                break;

            // Tilt response
            case ROT_RES_TILT:
                responseRotTilt((uint16_t)tmp.mid(8, 4).toInt(0, 16) / ROT_RATIO_TILT);
                break;

            }
        }
        else
        {
            qDebug() << "Wrong answer";
        }
    }
}

void Rotator::readRotPosition()
{
    queryRotPan();
    queryRotTilt();
}

void Rotator::queryRotPan()
{
    sendCommandToRotator(ROT_QUE_PAN);
    readResponseFromRotator();
}

void Rotator::queryRotTilt()
{
    sendCommandToRotator(ROT_QUE_TILT);
    readResponseFromRotator();
}

void Rotator::responseRotPan(int32_t respPan)
{
    respPan -= zeroPanOffset;

    if (respPan > 18000)
        respPan = respPan - 36000;

    rotPan = respPan;

    emit sendRotPan("Pan: " + QString::number(rotPan / 100.0, 'f', 2));

    if (!scanByStep && scanIsRunning && scanStarted)
    {
        int desiredPan;
        if (scanPanCur == scanPanEnd)
            desiredPan = scanPanBegin + scanIter * scanPanStep;
        else
            desiredPan = scanPanEnd - scanIter * scanPanStep;

        if (rotPan - desiredPan == 0)
        {
            scanIter++;
        }
    }
}

void Rotator::responseRotTilt(int32_t respTilt)
{
    respTilt += zeroTiltOffset;

    rotTilt = -respTilt;

    emit sendRotTilt("Tilt: " + QString::number(rotTilt / 100.0, 'f', 2));
}

void Rotator::writeRotPosition(int16_t pan, int16_t tilt)
{
    if (pan < -panLimit || pan > panLimit || tilt < -tiltLimit || tilt > tiltLimit)
        return;

    writeRotPan(pan);
    writeRotTilt(tilt);
    sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
    rotCommand = SET_POS;
}

void Rotator::writeRotPan(int16_t pan)
{
    pan += zeroPanOffset;

    // Unsigned
    uint16_t uPan = pan;

    if (pan < 0)
        uPan = 36000 + pan;

    uPan = (uint16_t)roundf((float)uPan * ROT_RATIO_PAN);

    sendCommandToRotator(ROT_LOAD_PAN, uPan / 256, uPan % 256);
}

void Rotator::writeRotTilt(int16_t tilt)
{
    tilt += zeroTiltOffset;
    tilt = -tilt;

    // Unsigned
    uint16_t uTilt = tilt;

    uTilt = (uint16_t)roundf((float)uTilt * ROT_RATIO_TILT);

    sendCommandToRotator(ROT_LOAD_TILT, uTilt / 256, uTilt % 256);
}

void Rotator::startScan(int16_t panBegin, int16_t panEnd, int16_t panStep, int16_t tiltBegin, int16_t tiltEnd, int16_t tiltStep)
{
    if (panBegin < -panLimit || panBegin > panLimit ||
        panEnd < -panLimit || panEnd > panLimit ||
        tiltBegin < -tiltLimit || tiltBegin > tiltLimit ||
        tiltEnd < -tiltLimit || tiltEnd > tiltLimit)
    {
        stopScan();
        return;
    }

    scanPanBegin = panBegin;
    scanPanEnd = panEnd;
    scanPanStep = panStep;
    scanTiltBegin = tiltBegin;
    scanTiltEnd = tiltEnd;
    scanTiltStep = tiltStep;

    scanIsRunning = true;
    scanStarted = false;
    scanPanDir = true;
    scanPanCur = scanPanBegin;
    scanTiltCur = scanTiltBegin;
    scanIter = 0;

    writeRotPosition(scanPanCur, scanTiltCur);
}

void Rotator::scan()
{
    if (scanIsRunning)
    {
        if (!scanByStep)
        {
            switch (rotCommand)
            {

            case NONE:
                qDebug() << "DONE NONE";
                break;

            case SET_POS:
                scanStarted = true;
                scanPanCur = scanPanEnd;
                writeRotPan(scanPanCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x1E);
                rotCommand = SET_PAN;
                break;

            case SET_PAN:
                if (scanTiltCur >= scanTiltEnd)
                {
                    stopScan();
                    return;
                }

                scanTiltCur += scanTiltStep;
                writeRotTilt(scanTiltCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                rotCommand = SET_TILT;
                scanIter = 0;
                break;

            case SET_TILT:
                if (scanPanCur == scanPanEnd)
                    scanPanCur = scanPanBegin;
                else
                    scanPanCur = scanPanEnd;

                writeRotPan(scanPanCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x1E);
                rotCommand = SET_PAN;
                break;

            }
        }
        else
        {
            switch (rotCommand)
            {

            case NONE:
                qDebug() << "DONE NONE";
                break;

            case SET_POS:
                scanStarted = true;

                scanPanCur += scanPanStep;
                writeRotPan(scanPanCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                rotCommand = SET_PAN;
                break;

            case SET_PAN:
                if (scanPanDir)
                    scanPanCur += scanPanStep;
                else
                    scanPanCur -= scanPanStep;

                if (scanPanDir && scanPanCur > scanPanEnd)
                {
                    scanPanDir = false;
                    scanPanCur = scanPanEnd;
                    scanTiltCur += scanTiltStep;
                    if (scanTiltCur > scanTiltEnd)
                    {
                        stopScan();
                        return;
                    }
                    writeRotTilt(scanTiltCur);
                    sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                    rotCommand = SET_TILT;
                    break;
                }
                else if (!scanPanDir && scanPanCur < scanPanBegin)
                {
                    scanPanDir = true;
                    scanPanCur = scanPanBegin;
                    scanTiltCur += scanTiltStep;
                    if (scanTiltCur > scanTiltEnd)
                    {
                        stopScan();
                        return;
                    }
                    writeRotTilt(scanTiltCur);
                    sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                    rotCommand = SET_TILT;
                    break;
                }

                writeRotPan(scanPanCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                rotCommand = SET_PAN;
                break;

            case SET_TILT:
                if (scanPanDir)
                    scanPanCur += scanPanStep;
                else
                    scanPanCur -= scanPanStep;

                writeRotPan(scanPanCur);
                sendCommandToRotator(ROT_SET_POS, 0x00, 0x00);
                rotCommand = SET_PAN;
                break;
            }
        }
    }
}

void Rotator::rotateLeft()
{
    if (!scanIsRunning)
        sendCommandToRotator(ROT_LEFT, speedPan, speedTilt);
}

void Rotator::rotateRight()
{
    if (!scanIsRunning)
        sendCommandToRotator(ROT_RIGHT, speedPan, speedTilt);
}

void Rotator::rotateUp()
{
    if (!scanIsRunning)
        sendCommandToRotator(ROT_UP, speedPan, speedTilt);
}

void Rotator::rotateDown()
{
    if (!scanIsRunning)
        sendCommandToRotator(ROT_DOWN, speedPan, speedTilt);
}

void Rotator::rotateStop()
{
    if (!scanIsRunning)
        sendCommandToRotator(ROT_STOP);
}

void Rotator::stepLeft()
{
    if (!scanIsRunning)
    {
        int16_t newRotPan = rotPan - rotPanStep;
        if (newRotPan >= -panLimit)
            writeRotPosition(newRotPan, rotTilt);
    }
}

void Rotator::stepRight()
{
    if (!scanIsRunning)
    {
        int16_t newRotPan = rotPan + rotPanStep;
        if (newRotPan <= panLimit)
            writeRotPosition(newRotPan, rotTilt);
    }
}

void Rotator::stepUp()
{
    if (!scanIsRunning)
    {
        int16_t newRotTilt = rotTilt - rotTiltStep;
        if (newRotTilt >= -tiltLimit)
            writeRotPosition(rotPan, newRotTilt);
    }
}

void Rotator::stepDown()
{
    if (!scanIsRunning)
    {
        int16_t newRotTilt = rotTilt + rotTiltStep;
        if (newRotTilt <= tiltLimit)
            writeRotPosition(rotPan, newRotTilt);
    }
}

void Rotator::stopScan()
{
    scanIsRunning = false;
    emit stoppingScan();
}

void Rotator::stopScanFromMain()
{
    scanIsRunning = false;
}

void Rotator::left()
{
    if (rotateByStep)
        stepLeft();
    else
        rotateLeft();
}

void Rotator::right()
{
    if (rotateByStep)
        stepRight();
    else
        rotateRight();
}

void Rotator::down()
{
    if (rotateByStep)
        stepDown();
    else
        rotateDown();
}

void Rotator::up()
{
    if (rotateByStep)
        stepUp();
    else
        rotateUp();
}

void Rotator::stop()
{
    if (!rotateByStep)
        rotateStop();
}
