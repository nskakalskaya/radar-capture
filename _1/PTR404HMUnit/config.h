#ifndef CONFIG_H
#define CONFIG_H

#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QColor>
#include <QVector>
#include <QDebug>

#define MAXNSWEEP 1

struct generalSettingsStruct
{
    bool scanLoop;
    bool rotateByStep;
    bool scanByStep;
};

struct rotatorSettingsStruct
{
    QString rotIPAddress;
    uint16_t rotPort;
    uint8_t rotAddr;
    uint8_t speedPan;
    uint8_t speedTilt;
    int16_t zeroPanOffset;
    int16_t zeroTiltOffset;
    int16_t panLimit;
    int16_t tiltLimit;
    int16_t rotatePanStep;
    int16_t rotateTiltStep;
};

class ConfigBase
{
    QSettings *cfgFile;

    generalSettingsStruct generalStgs, dfltGeneralStgs;
    rotatorSettingsStruct rotatorStgs, dfltRotatorStgs;

public:
    ConfigBase(QString iniName);
    ~ConfigBase();

    void restoreDefaultCfgFile();
    void saveCfgFile();


// -------------------------------------------------------------------
// Getters
// -------------------------------------------------------------------

    //General
    bool getScanLoop() {return generalStgs.scanLoop;}
    bool getRotateByStep() {return generalStgs.rotateByStep;}
    bool getScanByStep() {return generalStgs.scanByStep;}

    // Rotator
    QString getRotIPAddress() {return rotatorStgs.rotIPAddress;}
    uint16_t getRotPort() {return rotatorStgs.rotPort;}
    uint8_t getRotAddr() {return rotatorStgs.rotAddr;}
    uint8_t getSpeedPan() {return rotatorStgs.speedPan;}
    uint8_t getSpeedTilt() {return rotatorStgs.speedTilt;}
    int16_t getZeroPanOffset() {return rotatorStgs.zeroPanOffset;}
    int16_t getZeroTiltOffset() {return rotatorStgs.zeroTiltOffset;}
    int16_t getPanLimit() {return rotatorStgs.panLimit;}
    int16_t getTiltLimit() {return rotatorStgs.tiltLimit;}
    int16_t getRotatePanStep() {return rotatorStgs.rotatePanStep;}
    int16_t getRotateTiltStep() {return rotatorStgs.rotateTiltStep;}


// -------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------

    //General
    void setScanLoop(bool val) {generalStgs.scanLoop = val;}
    void setRotateByStep(bool val) {generalStgs.rotateByStep = val;}
    void setScanByStep(bool val) {generalStgs.scanByStep = val;}

    // Rotator
    void setRotIPAddress(QString val) {rotatorStgs.rotIPAddress = val;}
    void setRotPort(uint16_t val) {rotatorStgs.rotPort = val;}
    void setRotAddr(uint8_t val) {rotatorStgs.rotAddr = val;}
    void setSpeedPan(uint8_t val) {rotatorStgs.speedPan = val;}
    void setSpeedTilt(uint8_t val) {rotatorStgs.speedTilt = val;}
    void setZeroPanOffset(int16_t val) {rotatorStgs.zeroPanOffset = val;}
    void setZeroTiltOffset(int16_t val) {rotatorStgs.zeroTiltOffset = val;}
    void setPanLimit(int16_t val) {rotatorStgs.panLimit = val;}
    void setTiltLimit(int16_t val) {rotatorStgs.tiltLimit = val;}
    void setRotatePanStep(int16_t val) {rotatorStgs.rotatePanStep = val;}
    void setRotateTiltStep(int16_t val) {rotatorStgs.rotateTiltStep = val;}

};

#endif // CONFIG_H
