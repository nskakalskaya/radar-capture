#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T10:32:36
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11

TARGET = PTR404HMUnit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    rotator.cpp \
    config.cpp \
    settingswidget.cpp


HEADERS  += mainwindow.h \
    rotator.h \
    config.h \
    settingswidget.h
