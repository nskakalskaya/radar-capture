#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QtGui>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QKeyEvent>
#include <QSlider>

#include "config.h"

class SettingsWidget : public QWidget
{
    Q_OBJECT

    ConfigBase *cfgPtr;

    // General
    QCheckBox *scanLoopCheckBox;
    QCheckBox *rotateByStepCheckBox;
    QCheckBox *scanByStepCheckBox;
    QLineEdit *rotatePanStepEdit;
    QLineEdit *rotateTiltStepEdit;

    // Rotator
    QLineEdit *ipAddressEdit1, *ipAddressEdit2, *ipAddressEdit3, *ipAddressEdit4;
    QLineEdit *rotPortEdit;
    QLineEdit *devAddressEdit;
    QSlider *speedPanSlider;
    QSlider *speedTiltSlider;
    QLineEdit *zeroPanOffsetEdit;
    QLineEdit *zeroTiltOffsetEdit;
    QLineEdit *panLimitEdit;
    QLineEdit *tiltLimitEdit;

    void applyChanges();
    void restoreDefaultSettings();
    void updateSettingsFromCfg();

protected:
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);

public:
    explicit SettingsWidget(ConfigBase *cfgPtr, QWidget *parent = NULL);
    ~SettingsWidget();


signals:
    void updateSettings();

};

#endif // SETTINGSWIDGET_H
