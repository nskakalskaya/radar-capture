#include "config.h"

ConfigBase::ConfigBase(QString iniName)
{
    dfltGeneralStgs.scanLoop = false;
    dfltGeneralStgs.rotateByStep = true;
    dfltGeneralStgs.scanByStep = true;
    generalStgs = dfltGeneralStgs;

    dfltRotatorStgs.rotIPAddress = "192.168.10.190";
    dfltRotatorStgs.rotPort = 6000;
    dfltRotatorStgs.rotAddr = 0x01;
    dfltRotatorStgs.speedPan = 0x32;
    dfltRotatorStgs.speedTilt = 0x32;
    dfltRotatorStgs.zeroPanOffset = 18000;
    dfltRotatorStgs.zeroTiltOffset = -9000;
    dfltRotatorStgs.panLimit = 10000;
    dfltRotatorStgs.tiltLimit = 3000;
    dfltRotatorStgs.rotatePanStep = 100;
    dfltRotatorStgs.rotateTiltStep = 100;
    rotatorStgs = dfltRotatorStgs;

    cfgFile = new QSettings(iniName, QSettings::IniFormat);
//    QFileInfo fi("C:/Users/i.negru/Documents/_BUILDS/build-PTR404HMUnitControlOnly_MinGW32/" + iniName);
    QFileInfo fi(iniName);
    if (fi.exists())
    {
        cfgFile->beginGroup("GENERAL");
            generalStgs.scanLoop = cfgFile->value("scan_loop", dfltGeneralStgs.scanLoop).toBool();
            generalStgs.rotateByStep = cfgFile->value("rotate_by_step", dfltGeneralStgs.rotateByStep).toBool();
            generalStgs.scanByStep = cfgFile->value("scan_by_step", dfltGeneralStgs.scanByStep).toBool();
        cfgFile->endGroup();

        cfgFile->beginGroup("ROTATOR");
            rotatorStgs.rotIPAddress = cfgFile->value("ip_address", dfltRotatorStgs.rotIPAddress).toString();
            rotatorStgs.rotPort = cfgFile->value("port", dfltRotatorStgs.rotPort).toInt();
            rotatorStgs.rotAddr = cfgFile->value("dev_address", dfltRotatorStgs.rotAddr).toInt();
            rotatorStgs.speedPan = cfgFile->value("speed_pan", dfltRotatorStgs.speedPan).toInt();
            rotatorStgs.speedTilt = cfgFile->value("speed_tilt", dfltRotatorStgs.speedTilt).toInt();
            rotatorStgs.zeroPanOffset = cfgFile->value("zero_pan_offset", dfltRotatorStgs.zeroPanOffset).toInt();
            rotatorStgs.zeroTiltOffset = cfgFile->value("zero_tilt_offset", dfltRotatorStgs.zeroTiltOffset).toInt();
            rotatorStgs.panLimit = cfgFile->value("pan_limit", dfltRotatorStgs.panLimit).toInt();
            rotatorStgs.tiltLimit = cfgFile->value("tilt_limit", dfltRotatorStgs.tiltLimit).toInt();
            rotatorStgs.rotatePanStep = cfgFile->value("rotate_pan_step", dfltRotatorStgs.rotatePanStep).toInt();
            rotatorStgs.rotateTiltStep = cfgFile->value("rotate_tilt_step", dfltRotatorStgs.rotateTiltStep).toInt();
        cfgFile->endGroup();
    }
    else
        saveCfgFile();
}

void ConfigBase::saveCfgFile()
{
    cfgFile->remove("");

    cfgFile->beginGroup("GENERAL");
        cfgFile->setValue("scan_loop", generalStgs.scanLoop);
        cfgFile->setValue("rotate_by_step", generalStgs.rotateByStep);
        cfgFile->setValue("scan_by_step", generalStgs.scanByStep);
    cfgFile->endGroup();

    cfgFile->beginGroup("ROTATOR");
        cfgFile->setValue("ip_address", rotatorStgs.rotIPAddress);
        cfgFile->setValue("port", rotatorStgs.rotPort);
        cfgFile->setValue("dev_address", rotatorStgs.rotAddr);
        cfgFile->setValue("speed_pan", rotatorStgs.speedPan);
        cfgFile->setValue("speed_tilt", rotatorStgs.speedTilt);
        cfgFile->setValue("zero_pan_offset", rotatorStgs.zeroPanOffset);
        cfgFile->setValue("zero_tilt_offset", rotatorStgs.zeroTiltOffset);
        cfgFile->setValue("pan_limit", rotatorStgs.panLimit);
        cfgFile->setValue("tilt_limit", rotatorStgs.tiltLimit);
        cfgFile->setValue("rotate_pan_step", rotatorStgs.rotatePanStep);
        cfgFile->setValue("rotate_tilt_step", rotatorStgs.rotateTiltStep);
    cfgFile->endGroup();
}

void ConfigBase::restoreDefaultCfgFile()
{
    generalStgs = dfltGeneralStgs;
    rotatorStgs = dfltRotatorStgs;

    saveCfgFile();
}

ConfigBase::~ConfigBase()
{
    delete cfgFile;
}
