#ifndef ROTATOR_H
#define ROTATOR_H

#include <windows.h>
#include <stdint.h>

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QThread>
#include <QDebug>

#include "config.h"


// Rotation commands
const uint8_t ROT_LEFT          =   0x04;
const uint8_t ROT_RIGHT         =   0x02;
const uint8_t ROT_UP            =   0x10;
const uint8_t ROT_DOWN          =   0x08;

// Load and set coordinates/zoom commands
const uint8_t ROT_LOAD_PAN      =   0x81;
const uint8_t ROT_LOAD_TILT     =   0x83;
const uint8_t ROT_SET_POS       =   0x87;

// Query coordinates/zoom commands
const uint8_t ROT_QUE_PAN       =   0x51;
const uint8_t ROT_QUE_TILT      =   0x53;
const uint8_t ROT_QUE_ZOOM      =   0x55;

// Response coordinates/zoom commands
const uint8_t ROT_RES_PAN       =   0x61;
const uint8_t ROT_RES_TILT      =   0x63;
const uint8_t ROT_RES_CMD       =   0x7C;

// Stop rotation command
const uint8_t ROT_STOP          =   0x00;

// Position read timer interval
const uint16_t ROT_READ_TIMEOUT =   2;

// Steps / degrees ratio
const double ROT_RATIO_PAN      =   1.12;
const float ROT_RATIO_TILT      =   1.75;

enum rotCommandsEnum{NONE, SET_POS, SET_PAN, SET_TILT};

class Rotator : public QObject
{
    Q_OBJECT

    ConfigBase *cfgPtr;
    QUdpSocket *udpSocket;
    QByteArray rotDatagram;
    QHostAddress rotIPAddress;
    int16_t rotPort;
    uint8_t rotAddr;

    QTimer *readPosTimer;

    uint8_t speedPan, speedTilt;
    int16_t rotPan, rotTilt;
    int16_t zeroPanOffset, zeroTiltOffset;
    int16_t panLimit, tiltLimit;
    int16_t scanPanBegin, scanPanEnd, scanPanStep;
    int16_t scanTiltBegin, scanTiltEnd, scanTiltStep;
    int16_t rotPanStep, rotTiltStep;

    bool rotateByStep;
    bool scanByStep;
    bool rotatorIsConnected;
    bool scanIsRunning;                 // Scanning is in process
    bool scanStarted;                   // True - rotator is in begin position

    int16_t scanPanCur;
    int16_t scanPanPrev;
    int16_t scanTiltCur;
    uint16_t scanIter;
    rotCommandsEnum rotCommand;

    float distance;
    bool scanPanDir;

    void rotateLeft();
    void rotateRight();
    void rotateUp();
    void rotateDown();
    void rotateStop();

    void stepLeft();
    void stepRight();
    void stepUp();
    void stepDown();

    void writeRotPan(int16_t pan);
    void writeRotTilt(int16_t tilt);


public:
    explicit Rotator(ConfigBase *cfgPtr, QObject *parent = 0);
    ~Rotator();

    void createUDPSocket();
    void updateSettings();

    void setSpeedPan(uint8_t speed);
    void setSpeedTilt(uint8_t speed);

    void connectToRotator();
    void disconnectFromRotator();
    uint8_t sendCommandToRotator(const uint8_t command, uint8_t param1 = 0x00, uint8_t param2 = 0x00);
    void readResponseFromRotator();
    void readRotator();

    void readRotPosition();
    void queryRotPan();
    void queryRotTilt();
    void responseRotPan(int32_t respPan);
    void responseRotTilt(int32_t respTilt);

    void writeRotPosition(int16_t pan, int16_t tilt);

    void startScan(int16_t panBegin, int16_t panEnd, int16_t panStep, int16_t tiltBegin, int16_t tiltEnd, int16_t tiltStep);
    void scan();

    void stopScan();
    void stopScanFromMain();

//**********************************************************
// External interface
//**********************************************************

// Movements
    void left();
    void right();
    void down();
    void up();
    void stop();


signals:
    void rotatorConnected(bool success);
    void rotatorDisconnected(bool success);
    void passProcess();

    void sendRotPan(QString pan);
    void sendRotTilt(QString tilt);

    void stoppingScan();


};

#endif // ROTATOR_H
