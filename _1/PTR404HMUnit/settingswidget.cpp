#include "settingswidget.h"

#include <QLayout>
#include <QTabWidget>
#include <QColorDialog>
#include <QGroupBox>
#include <QDebug>


SettingsWidget::SettingsWidget(ConfigBase *cfg, QWidget *parent) :
    QWidget(parent, Qt::Dialog | Qt::WindowCloseButtonHint), cfgPtr(cfg)
{   
    // General
    scanLoopCheckBox = new QCheckBox("Scan Loop", this);
    rotateByStepCheckBox = new QCheckBox("Rotate By Step", this);
    scanByStepCheckBox = new QCheckBox("Scan By Step", this);

    QGridLayout *tabGeneralLayout = new QGridLayout;
    tabGeneralLayout->addWidget(scanLoopCheckBox, 0, 0);
    tabGeneralLayout->addWidget(rotateByStepCheckBox, 1, 0);
    tabGeneralLayout->addWidget(scanByStepCheckBox, 2, 0);

    QVBoxLayout *tabGeneralLayout1 = new QVBoxLayout;
    tabGeneralLayout1->addLayout(tabGeneralLayout);
    tabGeneralLayout1->addStretch(1);

    QWidget *tabGeneral = new QWidget(this);
    tabGeneral->setLayout(tabGeneralLayout1);


    // Rotator
    ipAddressEdit1 = new QLineEdit(this);
    ipAddressEdit2 = new QLineEdit(this);
    ipAddressEdit3 = new QLineEdit(this);
    ipAddressEdit4 = new QLineEdit(this);
    rotPortEdit = new QLineEdit(this);
    devAddressEdit = new QLineEdit(this);
    speedPanSlider = new QSlider(Qt::Horizontal, this);
    speedPanSlider->setMinimum(2);
    speedPanSlider->setMaximum(255);
    speedTiltSlider = new QSlider(Qt::Horizontal, this);
    speedTiltSlider->setMinimum(1);
    speedTiltSlider->setMaximum(254);
    zeroPanOffsetEdit = new QLineEdit(this);
    zeroTiltOffsetEdit = new QLineEdit(this);
    panLimitEdit = new QLineEdit(this);
    tiltLimitEdit = new QLineEdit(this);
    rotatePanStepEdit = new QLineEdit(this);
    rotateTiltStepEdit = new QLineEdit(this);

    QHBoxLayout *ipAddressLayout = new QHBoxLayout;
    ipAddressLayout->addWidget(new QLabel("IP address", this));
    ipAddressLayout->addWidget(ipAddressEdit1);
    ipAddressLayout->addWidget(ipAddressEdit2);
    ipAddressLayout->addWidget(ipAddressEdit3);
    ipAddressLayout->addWidget(ipAddressEdit4);

    QGridLayout *tabRotatorLayout = new QGridLayout;
    tabRotatorLayout->addWidget(new QLabel("Port", this), 0, 0);
    tabRotatorLayout->addWidget(new QLabel("Device Address", this), 1, 0);
    tabRotatorLayout->addWidget(new QLabel("Pan Speed", this), 2, 0);
    tabRotatorLayout->addWidget(new QLabel("Tilt Speed", this), 3, 0);
    tabRotatorLayout->addWidget(new QLabel("Pan Zero Offset", this), 4, 0);
    tabRotatorLayout->addWidget(new QLabel("Tilt Zero Offset", this), 5, 0);
    tabRotatorLayout->addWidget(new QLabel("Pan Limit", this), 6, 0);
    tabRotatorLayout->addWidget(new QLabel("Tilt Limit", this), 7, 0);
    tabRotatorLayout->addWidget(new QLabel("Pan Step", this), 8, 0);
    tabRotatorLayout->addWidget(new QLabel("Tilt Step", this), 9, 0);
    tabRotatorLayout->addWidget(rotPortEdit, 0, 1);
    tabRotatorLayout->addWidget(devAddressEdit, 1, 1);
    tabRotatorLayout->addWidget(speedPanSlider, 2, 1);
    tabRotatorLayout->addWidget(speedTiltSlider, 3, 1);
    tabRotatorLayout->addWidget(zeroPanOffsetEdit, 4, 1);
    tabRotatorLayout->addWidget(zeroTiltOffsetEdit, 5, 1);
    tabRotatorLayout->addWidget(panLimitEdit, 6, 1);
    tabRotatorLayout->addWidget(tiltLimitEdit, 7, 1);
    tabRotatorLayout->addWidget(rotatePanStepEdit, 8, 1);
    tabRotatorLayout->addWidget(rotateTiltStepEdit, 9, 1);

    QVBoxLayout *tabRotatorLayout1 = new QVBoxLayout;
    tabRotatorLayout1->addLayout(ipAddressLayout);
    tabRotatorLayout1->addLayout(tabRotatorLayout);
    tabRotatorLayout1->addStretch(1);

    QWidget *tabRotator = new QWidget(this);
    tabRotator->setLayout(tabRotatorLayout1);



    QPushButton *restoreDefaultButton = new QPushButton("Restore Defaults", this);

    QTabWidget *tabsWidget = new QTabWidget(this);
    tabsWidget->addTab(tabGeneral, "General");
    tabsWidget->addTab(tabRotator, "Rotator");

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(tabsWidget);
    mainLayout->addWidget(restoreDefaultButton, Qt::AlignCenter);

    connect(restoreDefaultButton, &QPushButton::clicked, this, &SettingsWidget::restoreDefaultSettings);

    setLayout(mainLayout);
    setFixedWidth(270);
    setFixedHeight(410);

    updateSettingsFromCfg();
}

void SettingsWidget::applyChanges()
{
    // General
    cfgPtr->setScanLoop(scanLoopCheckBox->isChecked());
    cfgPtr->setRotateByStep(rotateByStepCheckBox->isChecked());
    cfgPtr->setScanByStep(scanByStepCheckBox->isChecked());

    // Rotator
    QStringList ipList;
    ipList.append(ipAddressEdit1->text());
    ipList.append(ipAddressEdit2->text());
    ipList.append(ipAddressEdit3->text());
    ipList.append(ipAddressEdit4->text());
    cfgPtr->setRotIPAddress(ipList.join("."));
    cfgPtr->setRotPort(rotPortEdit->text().toInt());
    cfgPtr->setRotAddr(devAddressEdit->text().toInt());
    cfgPtr->setSpeedPan(speedPanSlider->value());
    cfgPtr->setSpeedTilt(speedTiltSlider->value());
    cfgPtr->setZeroPanOffset(zeroPanOffsetEdit->text().toFloat() * 100);
    cfgPtr->setZeroTiltOffset(zeroTiltOffsetEdit->text().toFloat() * 100);
    cfgPtr->setPanLimit(panLimitEdit->text().toFloat() * 100);
    cfgPtr->setTiltLimit(tiltLimitEdit->text().toFloat() * 100);
    cfgPtr->setRotatePanStep(rotatePanStepEdit->text().toFloat() * 100);
    cfgPtr->setRotateTiltStep(rotateTiltStepEdit->text().toFloat() * 100);

    cfgPtr->saveCfgFile();
    emit updateSettings();
}

void SettingsWidget::restoreDefaultSettings()
{
    cfgPtr->restoreDefaultCfgFile();
    updateSettingsFromCfg();
    emit updateSettings();
}

void SettingsWidget::updateSettingsFromCfg()
{
    // General
    scanLoopCheckBox->setChecked(cfgPtr->getScanLoop());
    rotateByStepCheckBox->setChecked(cfgPtr->getRotateByStep());
    scanByStepCheckBox->setChecked(cfgPtr->getScanByStep());

    // Rotator
    QString ipAddress = cfgPtr->getRotIPAddress();
    QStringList ipAddressList = ipAddress.split(".");
    ipAddressEdit1->setText(ipAddressList.at(0));
    ipAddressEdit2->setText(ipAddressList.at(1));
    ipAddressEdit3->setText(ipAddressList.at(2));
    ipAddressEdit4->setText(ipAddressList.at(3));
    rotPortEdit->setText(QString::number(cfgPtr->getRotPort()));
    devAddressEdit->setText(QString::number(cfgPtr->getRotAddr()));
    speedPanSlider->setValue(cfgPtr->getSpeedPan());
    speedTiltSlider->setValue(cfgPtr->getSpeedTilt());
    zeroPanOffsetEdit->setText(QString::number((float)cfgPtr->getZeroPanOffset() / 100.0f, 'f', 2));
    zeroTiltOffsetEdit->setText(QString::number((float)cfgPtr->getZeroTiltOffset() / 100.0f, 'f', 2));
    panLimitEdit->setText(QString::number((float)cfgPtr->getPanLimit() / 100.0f, 'f', 2));
    tiltLimitEdit->setText(QString::number((float)cfgPtr->getTiltLimit() / 100.0f, 'f', 2));
    rotatePanStepEdit->setText(QString::number((float)cfgPtr->getRotatePanStep() / 100.0f, 'f', 2));
    rotateTiltStepEdit->setText(QString::number((float)cfgPtr->getRotateTiltStep() / 100.0f, 'f', 2));
}

void SettingsWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        this->close();
}

void SettingsWidget::closeEvent(QCloseEvent *event)
{
    applyChanges();
}

SettingsWidget::~SettingsWidget()
{
    cfgPtr = NULL;
}

