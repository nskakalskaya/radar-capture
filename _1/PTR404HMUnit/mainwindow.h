#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QPushButton>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QGroupBox>
#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <QLayout>
#include <QMessageBox>
#include <QThread>
#include <QFileDialog>
#include <QAction>
#include <QMenuBar>
#include <QStatusBar>
#include <QMenu>
#include <QInputDialog>

#include "rotator.h"
#include "settingswidget.h"



// Bit masks for enabling/disabling groupboxes
#define GRMASK_CONNECT      0x01
#define GRMASK_CUR_POS      0x02
#define GRMASK_MOVE         0x04
#define GRMASK_SET_POS      0x08
#define GRMASK_SCAN         0x10
#define GRMASK_SCAN_SETT    0x20
#define GRMASK_SETTINGS     0x40
#define GRMASK_ALL          0xFF


class MainWindow : public QMainWindow
{
    Q_OBJECT

    QMenu *fileMenu;
    QAction *fileQuitAct;
    QMenu *settingsMenu;
    QAction *settingsConfAct;

    void createActions();
    void createMenus();

    QLabel *statusBarLabel;

    ConfigBase *cfgMain;
    SettingsWidget *settings;

/* -------------- ROTATOR --------------- */
    Rotator *rotator;
    QThread rotatorThread;
    bool rotatorIsConnected;

    QPushButton *rotConnectButton;
    QPushButton *rotLeftButton, *rotRightButton, *rotUpButton, *rotDownButton;
    QPushButton *setPositionButton;

    QLabel *currentPanLabel, *currentTiltLabel;
    QLineEdit *setPanEdit, *setTiltEdit;


    QGroupBox *connectGroupBox;
    QGroupBox *currentPositionGroupBox;
    QGroupBox *movementGroupBox;
    QGroupBox *setPositionGroupBox;
    QGroupBox *scanGroupBox;
    QGroupBox *scanSettingsGroupBox;

    QPushButton *scanButton;
    bool scanIsRunning;
    bool scanIsLooped;
    bool scanResume;
    QLineEdit *setScanPanBeginEdit, *setScanPanEndEdit, *setScanPanStepEdit;
    QLineEdit *setScanTiltBeginEdit, *setScanTiltEndEdit, *setScanTiltStepEdit;

    void updateSettings();

    void startScan();
    void setGroupsEnabled(uint8_t flags);       // Set enable groupboxes according to flags (groupboxes that are not in the flags will be disabled)

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent * event);



public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();


signals:
    void sendConnectRotator();
    void sendDisconnectRotator();
    void startScanSignal(int16_t panBegin, int16_t panEnd, int16_t panStep, int16_t tiltBegin, int16_t tiltEnd, int16_t tiltStep);


public slots:
    void rotatorConnected(bool success);
    void rotatorDisconnected(bool success);
    void errorMessageBox(QString str);
    void connectRotator();
    void setPositionOfRotator();

    void stopScanFromRotator();
    void terminateScan();

    void setScanLoop(bool isLooped);



};

#endif // MAINWINDOW_H
