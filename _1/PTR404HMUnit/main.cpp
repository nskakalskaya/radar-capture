#include <QApplication>
#include <QDesktopWidget>

#include "mainwindow.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QRect rect = QApplication::desktop()->availableGeometry();
    int wWidth = 465;
    int wHeight = 355;

    MainWindow w;
    w.setFixedSize(wWidth, wHeight);
    w.move((rect.width() - wWidth) / 2, (rect.height() - wHeight) / 2);
    w.show();

    return a.exec();
}
