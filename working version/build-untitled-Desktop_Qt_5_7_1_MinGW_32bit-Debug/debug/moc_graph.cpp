/****************************************************************************
** Meta object code from reading C++ file 'graph.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../untitled/graph.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'graph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Graph_t {
    QByteArrayData data[36];
    char stringdata0[413];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Graph_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Graph_t qt_meta_stringdata_Graph = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Graph"
QT_MOC_LITERAL(1, 6, 21), // "rangeChangeToSpinBoxX"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "QCPRange"
QT_MOC_LITERAL(4, 38, 5), // "range"
QT_MOC_LITERAL(5, 44, 21), // "rangeChangeToSpinBoxY"
QT_MOC_LITERAL(6, 66, 15), // "sendBorderValue"
QT_MOC_LITERAL(7, 82, 4), // "isUp"
QT_MOC_LITERAL(8, 87, 9), // "sendValue"
QT_MOC_LITERAL(9, 97, 15), // "QVector<double>"
QT_MOC_LITERAL(10, 113, 2), // "uf"
QT_MOC_LITERAL(11, 116, 2), // "xf"
QT_MOC_LITERAL(12, 119, 5), // "reVec"
QT_MOC_LITERAL(13, 125, 5), // "imVec"
QT_MOC_LITERAL(14, 131, 21), // "sendBorderValueInside"
QT_MOC_LITERAL(15, 153, 5), // "sendY"
QT_MOC_LITERAL(16, 159, 5), // "sendX"
QT_MOC_LITERAL(17, 165, 11), // "updateGraph"
QT_MOC_LITERAL(18, 177, 10), // "getFourier"
QT_MOC_LITERAL(19, 188, 8), // "getSweep"
QT_MOC_LITERAL(20, 197, 13), // "getSweepValue"
QT_MOC_LITERAL(21, 211, 12), // "getVectorsUp"
QT_MOC_LITERAL(22, 224, 10), // "getVectorX"
QT_MOC_LITERAL(23, 235, 10), // "getVectorY"
QT_MOC_LITERAL(24, 246, 14), // "getVectorsDown"
QT_MOC_LITERAL(25, 261, 6), // "getRaw"
QT_MOC_LITERAL(26, 268, 5), // "check"
QT_MOC_LITERAL(27, 274, 13), // "getEmbFourier"
QT_MOC_LITERAL(28, 288, 9), // "timerStop"
QT_MOC_LITERAL(29, 298, 10), // "timerStart"
QT_MOC_LITERAL(30, 309, 19), // "getRangeApplySignal"
QT_MOC_LITERAL(31, 329, 19), // "fromSpinBoxToXScale"
QT_MOC_LITERAL(32, 349, 19), // "fromSpinBoxToYScale"
QT_MOC_LITERAL(33, 369, 11), // "getNewRange"
QT_MOC_LITERAL(34, 381, 15), // "onXRangeChanged"
QT_MOC_LITERAL(35, 397, 15) // "onYRangeChanged"

    },
    "Graph\0rangeChangeToSpinBoxX\0\0QCPRange\0"
    "range\0rangeChangeToSpinBoxY\0sendBorderValue\0"
    "isUp\0sendValue\0QVector<double>\0uf\0xf\0"
    "reVec\0imVec\0sendBorderValueInside\0"
    "sendY\0sendX\0updateGraph\0getFourier\0"
    "getSweep\0getSweepValue\0getVectorsUp\0"
    "getVectorX\0getVectorY\0getVectorsDown\0"
    "getRaw\0check\0getEmbFourier\0timerStop\0"
    "timerStart\0getRangeApplySignal\0"
    "fromSpinBoxToXScale\0fromSpinBoxToYScale\0"
    "getNewRange\0onXRangeChanged\0onYRangeChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Graph[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       5,    1,  112,    2, 0x06 /* Public */,
       6,    6,  115,    2, 0x06 /* Public */,
      14,    4,  128,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    0,  137,    2, 0x0a /* Public */,
      18,    1,  138,    2, 0x0a /* Public */,
      19,    1,  141,    2, 0x0a /* Public */,
      21,    2,  144,    2, 0x0a /* Public */,
      24,    2,  149,    2, 0x0a /* Public */,
      25,    1,  154,    2, 0x0a /* Public */,
      27,    1,  157,    2, 0x0a /* Public */,
      28,    0,  160,    2, 0x0a /* Public */,
      29,    0,  161,    2, 0x0a /* Public */,
      30,    0,  162,    2, 0x0a /* Public */,
      31,    1,  163,    2, 0x0a /* Public */,
      32,    1,  166,    2, 0x0a /* Public */,
      33,    0,  169,    2, 0x08 /* Private */,
      34,    1,  170,    2, 0x08 /* Private */,
      35,    1,  173,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool, QMetaType::Double, 0x80000000 | 9, 0x80000000 | 9, 0x80000000 | 9, 0x80000000 | 9,    7,    8,   10,   11,   12,   13,
    QMetaType::Void, QMetaType::Bool, QMetaType::Double, 0x80000000 | 9, 0x80000000 | 9,    7,    8,   15,   16,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::UShort,   20,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 9,   22,   23,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 9,   22,   23,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void Graph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Graph *_t = static_cast<Graph *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->rangeChangeToSpinBoxX((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 1: _t->rangeChangeToSpinBoxY((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 2: _t->sendBorderValue((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< QVector<double>(*)>(_a[3])),(*reinterpret_cast< QVector<double>(*)>(_a[4])),(*reinterpret_cast< QVector<double>(*)>(_a[5])),(*reinterpret_cast< QVector<double>(*)>(_a[6]))); break;
        case 3: _t->sendBorderValueInside((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< QVector<double>(*)>(_a[3])),(*reinterpret_cast< QVector<double>(*)>(_a[4]))); break;
        case 4: _t->updateGraph(); break;
        case 5: _t->getFourier((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->getSweep((*reinterpret_cast< quint16(*)>(_a[1]))); break;
        case 7: _t->getVectorsUp((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 8: _t->getVectorsDown((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 9: _t->getRaw((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->getEmbFourier((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->timerStop(); break;
        case 12: _t->timerStart(); break;
        case 13: _t->getRangeApplySignal(); break;
        case 14: _t->fromSpinBoxToXScale((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 15: _t->fromSpinBoxToYScale((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 16: _t->getNewRange(); break;
        case 17: _t->onXRangeChanged((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 18: _t->onYRangeChanged((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 5:
            case 4:
            case 3:
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Graph::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::rangeChangeToSpinBoxX)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Graph::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::rangeChangeToSpinBoxY)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Graph::*_t)(bool , double , QVector<double> , QVector<double> , QVector<double> , QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::sendBorderValue)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Graph::*_t)(bool , double , QVector<double> , QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::sendBorderValueInside)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject Graph::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Graph.data,
      qt_meta_data_Graph,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Graph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Graph::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Graph.stringdata0))
        return static_cast<void*>(const_cast< Graph*>(this));
    return QWidget::qt_metacast(_clname);
}

int Graph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void Graph::rangeChangeToSpinBoxX(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Graph::rangeChangeToSpinBoxY(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Graph::sendBorderValue(bool _t1, double _t2, QVector<double> _t3, QVector<double> _t4, QVector<double> _t5, QVector<double> _t6)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Graph::sendBorderValueInside(bool _t1, double _t2, QVector<double> _t3, QVector<double> _t4)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
