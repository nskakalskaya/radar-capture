
#ifndef ELEMENTS_H
#define ELEMENTS_H

#include <QObject>
#include <QWidget>
#include "qcustomplot.h"

#define XDEFAULTRANGE 1025
#define YDEFAULTRANGE 300

class Elements : public QWidget
{
    Q_OBJECT

public:
    explicit Elements(QWidget *parent = 0);
    ~Elements();
protected:

private:
    QLabel *portNumberLabel, *harmonicValueLabel, *frameSizeLabel, *framesNumberLabel, *showNewMaxUpLabel, *showNewMaxDownLabel, *rawDataLabel, *fourierLabel, *embFLabel;
    QLabel *xAxisLabel, *yAxisLabel;
    QLineEdit *readPortLineEdit, *readHNumberLineEdit, *readSizeOfAFrameLineEdit, *readNumberOfFramesLineEdit, *showNewMaxUpLineEdit, *showNewMaxDownLineEdit;
    QCheckBox *rawDataCheckBox, *fourierCheckBox, *embeddedFourierCheckBox;
    QPushButton *testPushButton, *setDefaultRangeButton, *applyButton, *stopButton, *saveConfigButton, *restoreRangeButton, *saveRangeButton, *startStopSavingButton, *launchSavedButton;
    QDoubleSpinBox *xScaleChangedSpinBox, *yScaleChangedSpinBox;
    QDoubleSpinBox *xScaleChangedSpinBox2, *yScaleChangedSpinBox2;
    QGroupBox *rightGroupBox(), *leftGroupBox(), *centerGroupBox(), *recordingGroupBox();
    qreal harmonicNumber, defaultHarmonicValue;
    quint32 portNumber, sizeOfPacket, numberOfPackets;
    bool doRaw = false, doFourier = false, isRunning = true, embFourier = false, ifFourier = false, isRecording = false, isPlaying = false;

signals:
    void sendFourierSignal(bool check);
    void sendRawSignal(bool check);
    void timerStops();
    void timerStarts();
    void applySignal();
    void sendRangeSignalX(const QCPRange &range);
    void sendRangeSignalY(const QCPRange &range);
    void changeHarmonicValue(qreal harmonic);
    void changeFrameSize(quint32 size);
    void changeNumberFrames(quint32 num);
    void sendResultVector(QVector <double> resultVector);
    void sendEmbFourierSignal(bool check);
    void sendIsRecording(bool isRec);
    void sendPlaySignal(bool isPlay, QString filename);

public slots:
    void setPortNumber(quint32 portNumber);
    void setHarmonicNumber(qreal harmonic);
    void setFrameSize(quint32 frameSize);
    void setNumberOfPackets(quint32 numberFrames);
    void setNewMax(bool isUp, double newMax);
    void getNewScaleToSpinX(const QCPRange &range);
    void getNewScaleToSpinY(const QCPRange &range);

private slots:
    void doRawDataCheckBox(bool check);
    void doFourierCheckBox(bool check);
    void embeddedFourier(bool check);
 };

#endif // ELEMENTS_H
