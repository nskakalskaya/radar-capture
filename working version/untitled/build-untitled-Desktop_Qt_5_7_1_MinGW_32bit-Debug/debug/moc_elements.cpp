/****************************************************************************
** Meta object code from reading C++ file 'elements.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../elements.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'elements.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Elements_t {
    QByteArrayData data[34];
    char stringdata0[460];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Elements_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Elements_t qt_meta_stringdata_Elements = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Elements"
QT_MOC_LITERAL(1, 9, 17), // "sendFourierSignal"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 5), // "check"
QT_MOC_LITERAL(4, 34, 13), // "sendRawSignal"
QT_MOC_LITERAL(5, 48, 10), // "timerStops"
QT_MOC_LITERAL(6, 59, 11), // "timerStarts"
QT_MOC_LITERAL(7, 71, 11), // "applySignal"
QT_MOC_LITERAL(8, 83, 16), // "sendRangeSignalX"
QT_MOC_LITERAL(9, 100, 8), // "QCPRange"
QT_MOC_LITERAL(10, 109, 5), // "range"
QT_MOC_LITERAL(11, 115, 16), // "sendRangeSignalY"
QT_MOC_LITERAL(12, 132, 19), // "changeHarmonicValue"
QT_MOC_LITERAL(13, 152, 8), // "harmonic"
QT_MOC_LITERAL(14, 161, 15), // "changeFrameSize"
QT_MOC_LITERAL(15, 177, 4), // "size"
QT_MOC_LITERAL(16, 182, 18), // "changeNumberFrames"
QT_MOC_LITERAL(17, 201, 3), // "num"
QT_MOC_LITERAL(18, 205, 16), // "sendResultVector"
QT_MOC_LITERAL(19, 222, 15), // "QVector<double>"
QT_MOC_LITERAL(20, 238, 12), // "resultVector"
QT_MOC_LITERAL(21, 251, 20), // "sendEmbFourierSignal"
QT_MOC_LITERAL(22, 272, 13), // "setPortNumber"
QT_MOC_LITERAL(23, 286, 10), // "portNumber"
QT_MOC_LITERAL(24, 297, 17), // "setHarmonicNumber"
QT_MOC_LITERAL(25, 315, 12), // "setFrameSize"
QT_MOC_LITERAL(26, 328, 9), // "frameSize"
QT_MOC_LITERAL(27, 338, 18), // "setNumberOfPackets"
QT_MOC_LITERAL(28, 357, 12), // "numberFrames"
QT_MOC_LITERAL(29, 370, 18), // "getNewScaleToSpinX"
QT_MOC_LITERAL(30, 389, 18), // "getNewScaleToSpinY"
QT_MOC_LITERAL(31, 408, 17), // "doRawDataCheckBox"
QT_MOC_LITERAL(32, 426, 17), // "doFourierCheckBox"
QT_MOC_LITERAL(33, 444, 15) // "embeddedFourier"

    },
    "Elements\0sendFourierSignal\0\0check\0"
    "sendRawSignal\0timerStops\0timerStarts\0"
    "applySignal\0sendRangeSignalX\0QCPRange\0"
    "range\0sendRangeSignalY\0changeHarmonicValue\0"
    "harmonic\0changeFrameSize\0size\0"
    "changeNumberFrames\0num\0sendResultVector\0"
    "QVector<double>\0resultVector\0"
    "sendEmbFourierSignal\0setPortNumber\0"
    "portNumber\0setHarmonicNumber\0setFrameSize\0"
    "frameSize\0setNumberOfPackets\0numberFrames\0"
    "getNewScaleToSpinX\0getNewScaleToSpinY\0"
    "doRawDataCheckBox\0doFourierCheckBox\0"
    "embeddedFourier"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Elements[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  119,    2, 0x06 /* Public */,
       4,    1,  122,    2, 0x06 /* Public */,
       5,    0,  125,    2, 0x06 /* Public */,
       6,    0,  126,    2, 0x06 /* Public */,
       7,    0,  127,    2, 0x06 /* Public */,
       8,    1,  128,    2, 0x06 /* Public */,
      11,    1,  131,    2, 0x06 /* Public */,
      12,    1,  134,    2, 0x06 /* Public */,
      14,    1,  137,    2, 0x06 /* Public */,
      16,    1,  140,    2, 0x06 /* Public */,
      18,    1,  143,    2, 0x06 /* Public */,
      21,    1,  146,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      22,    1,  149,    2, 0x0a /* Public */,
      24,    1,  152,    2, 0x0a /* Public */,
      25,    1,  155,    2, 0x0a /* Public */,
      27,    1,  158,    2, 0x0a /* Public */,
      29,    1,  161,    2, 0x0a /* Public */,
      30,    1,  164,    2, 0x0a /* Public */,
      31,    1,  167,    2, 0x08 /* Private */,
      32,    1,  170,    2, 0x08 /* Private */,
      33,    1,  173,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::QReal,   13,
    QMetaType::Void, QMetaType::UInt,   15,
    QMetaType::Void, QMetaType::UInt,   17,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, QMetaType::Bool,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::UInt,   23,
    QMetaType::Void, QMetaType::QReal,   13,
    QMetaType::Void, QMetaType::UInt,   26,
    QMetaType::Void, QMetaType::UInt,   28,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,

       0        // eod
};

void Elements::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Elements *_t = static_cast<Elements *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendFourierSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->sendRawSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->timerStops(); break;
        case 3: _t->timerStarts(); break;
        case 4: _t->applySignal(); break;
        case 5: _t->sendRangeSignalX((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 6: _t->sendRangeSignalY((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 7: _t->changeHarmonicValue((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 8: _t->changeFrameSize((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 9: _t->changeNumberFrames((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 10: _t->sendResultVector((*reinterpret_cast< QVector<double>(*)>(_a[1]))); break;
        case 11: _t->sendEmbFourierSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->setPortNumber((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 13: _t->setHarmonicNumber((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 14: _t->setFrameSize((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 15: _t->setNumberOfPackets((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 16: _t->getNewScaleToSpinX((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 17: _t->getNewScaleToSpinY((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 18: _t->doRawDataCheckBox((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->doFourierCheckBox((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->embeddedFourier((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Elements::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendFourierSignal)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendRawSignal)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Elements::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::timerStops)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Elements::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::timerStarts)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Elements::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::applySignal)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendRangeSignalX)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendRangeSignalY)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::changeHarmonicValue)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::changeFrameSize)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::changeNumberFrames)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendResultVector)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (Elements::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Elements::sendEmbFourierSignal)) {
                *result = 11;
                return;
            }
        }
    }
}

const QMetaObject Elements::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Elements.data,
      qt_meta_data_Elements,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Elements::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Elements::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Elements.stringdata0))
        return static_cast<void*>(const_cast< Elements*>(this));
    return QWidget::qt_metacast(_clname);
}

int Elements::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void Elements::sendFourierSignal(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Elements::sendRawSignal(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Elements::timerStops()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void Elements::timerStarts()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void Elements::applySignal()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void Elements::sendRangeSignalX(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Elements::sendRangeSignalY(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Elements::changeHarmonicValue(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Elements::changeFrameSize(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Elements::changeNumberFrames(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Elements::sendResultVector(QVector<double> _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Elements::sendEmbFourierSignal(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_END_MOC_NAMESPACE
