/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[39];
    char stringdata0[453];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "sendPortAddress"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 10), // "portNumber"
QT_MOC_LITERAL(4, 39, 18), // "sendHarmonicNumber"
QT_MOC_LITERAL(5, 58, 14), // "harmonicNumber"
QT_MOC_LITERAL(6, 73, 13), // "sendFrameSize"
QT_MOC_LITERAL(7, 87, 12), // "sizeOfPacket"
QT_MOC_LITERAL(8, 100, 19), // "sendNumberOfPackets"
QT_MOC_LITERAL(9, 120, 15), // "numberOfPackets"
QT_MOC_LITERAL(10, 136, 17), // "startSocketSignal"
QT_MOC_LITERAL(11, 154, 9), // "sendSweep"
QT_MOC_LITERAL(12, 164, 8), // "gotSweep"
QT_MOC_LITERAL(13, 173, 13), // "sendVectorsUp"
QT_MOC_LITERAL(14, 187, 15), // "QVector<double>"
QT_MOC_LITERAL(15, 203, 3), // "xUp"
QT_MOC_LITERAL(16, 207, 3), // "yUp"
QT_MOC_LITERAL(17, 211, 15), // "sendVectorsDown"
QT_MOC_LITERAL(18, 227, 5), // "xDown"
QT_MOC_LITERAL(19, 233, 5), // "yDown"
QT_MOC_LITERAL(20, 239, 16), // "newHarmonicValue"
QT_MOC_LITERAL(21, 256, 8), // "harmonic"
QT_MOC_LITERAL(22, 265, 12), // "newFrameSize"
QT_MOC_LITERAL(23, 278, 4), // "size"
QT_MOC_LITERAL(24, 283, 15), // "newNumberFrames"
QT_MOC_LITERAL(25, 299, 6), // "number"
QT_MOC_LITERAL(26, 306, 12), // "receiveFrame"
QT_MOC_LITERAL(27, 319, 11), // "QByteArray&"
QT_MOC_LITERAL(28, 331, 5), // "Array"
QT_MOC_LITERAL(29, 337, 12), // "receiveSweep"
QT_MOC_LITERAL(30, 350, 16), // "QVector<qint16>&"
QT_MOC_LITERAL(31, 367, 5), // "sweep"
QT_MOC_LITERAL(32, 373, 10), // "sweepValue"
QT_MOC_LITERAL(33, 384, 13), // "receiveSweepF"
QT_MOC_LITERAL(34, 398, 17), // "QVector<quint16>&"
QT_MOC_LITERAL(35, 416, 6), // "getRaw"
QT_MOC_LITERAL(36, 423, 5), // "check"
QT_MOC_LITERAL(37, 429, 7), // "getEmbF"
QT_MOC_LITERAL(38, 437, 15) // "getFourierValue"

    },
    "MainWindow\0sendPortAddress\0\0portNumber\0"
    "sendHarmonicNumber\0harmonicNumber\0"
    "sendFrameSize\0sizeOfPacket\0"
    "sendNumberOfPackets\0numberOfPackets\0"
    "startSocketSignal\0sendSweep\0gotSweep\0"
    "sendVectorsUp\0QVector<double>\0xUp\0yUp\0"
    "sendVectorsDown\0xDown\0yDown\0"
    "newHarmonicValue\0harmonic\0newFrameSize\0"
    "size\0newNumberFrames\0number\0receiveFrame\0"
    "QByteArray&\0Array\0receiveSweep\0"
    "QVector<qint16>&\0sweep\0sweepValue\0"
    "receiveSweepF\0QVector<quint16>&\0getRaw\0"
    "check\0getEmbF\0getFourierValue"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x06 /* Public */,
       4,    1,  102,    2, 0x06 /* Public */,
       6,    1,  105,    2, 0x06 /* Public */,
       8,    1,  108,    2, 0x06 /* Public */,
      10,    1,  111,    2, 0x06 /* Public */,
      11,    1,  114,    2, 0x06 /* Public */,
      13,    2,  117,    2, 0x06 /* Public */,
      17,    2,  122,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      20,    1,  127,    2, 0x08 /* Private */,
      22,    1,  130,    2, 0x08 /* Private */,
      24,    1,  133,    2, 0x08 /* Private */,
      26,    1,  136,    2, 0x08 /* Private */,
      29,    2,  139,    2, 0x08 /* Private */,
      33,    2,  144,    2, 0x08 /* Private */,
      35,    1,  149,    2, 0x08 /* Private */,
      37,    1,  152,    2, 0x08 /* Private */,
      38,    1,  155,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::UInt,    3,
    QMetaType::Void, QMetaType::QReal,    5,
    QMetaType::Void, QMetaType::UInt,    7,
    QMetaType::Void, QMetaType::UInt,    9,
    QMetaType::Void, QMetaType::UInt,    3,
    QMetaType::Void, QMetaType::UShort,   12,
    QMetaType::Void, 0x80000000 | 14, 0x80000000 | 14,   15,   16,
    QMetaType::Void, 0x80000000 | 14, 0x80000000 | 14,   18,   19,

 // slots: parameters
    QMetaType::Void, QMetaType::QReal,   21,
    QMetaType::Void, QMetaType::UInt,   23,
    QMetaType::Void, QMetaType::UInt,   25,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void, 0x80000000 | 30, QMetaType::UShort,   31,   32,
    QMetaType::Void, 0x80000000 | 34, QMetaType::UShort,   31,   32,
    QMetaType::Void, QMetaType::Bool,   36,
    QMetaType::Void, QMetaType::Bool,   36,
    QMetaType::Void, QMetaType::Bool,   36,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendPortAddress((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 1: _t->sendHarmonicNumber((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->sendFrameSize((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 3: _t->sendNumberOfPackets((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 4: _t->startSocketSignal((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 5: _t->sendSweep((*reinterpret_cast< quint16(*)>(_a[1]))); break;
        case 6: _t->sendVectorsUp((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 7: _t->sendVectorsDown((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 8: _t->newHarmonicValue((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 9: _t->newFrameSize((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 10: _t->newNumberFrames((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 11: _t->receiveFrame((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 12: _t->receiveSweep((*reinterpret_cast< QVector<qint16>(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 13: _t->receiveSweepF((*reinterpret_cast< QVector<quint16>(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 14: _t->getRaw((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->getEmbF((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->getFourierValue((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendPortAddress)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(qreal );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendHarmonicNumber)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendFrameSize)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendNumberOfPackets)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(quint32 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::startSocketSignal)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(quint16 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendSweep)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QVector<double> , QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendVectorsUp)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QVector<double> , QVector<double> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendVectorsDown)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::sendPortAddress(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::sendHarmonicNumber(qreal _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::sendFrameSize(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::sendNumberOfPackets(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::startSocketSignal(quint32 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::sendSweep(quint16 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MainWindow::sendVectorsUp(QVector<double> _t1, QVector<double> _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::sendVectorsDown(QVector<double> _t1, QVector<double> _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_END_MOC_NAMESPACE
