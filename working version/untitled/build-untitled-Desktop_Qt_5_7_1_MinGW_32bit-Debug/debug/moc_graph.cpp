/****************************************************************************
** Meta object code from reading C++ file 'graph.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../graph.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'graph.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Graph_t {
    QByteArrayData data[25];
    char stringdata0[316];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Graph_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Graph_t qt_meta_stringdata_Graph = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Graph"
QT_MOC_LITERAL(1, 6, 21), // "rangeChangeToSpinBoxX"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "QCPRange"
QT_MOC_LITERAL(4, 38, 5), // "range"
QT_MOC_LITERAL(5, 44, 21), // "rangeChangeToSpinBoxY"
QT_MOC_LITERAL(6, 66, 11), // "updateGraph"
QT_MOC_LITERAL(7, 78, 10), // "getFourier"
QT_MOC_LITERAL(8, 89, 8), // "getSweep"
QT_MOC_LITERAL(9, 98, 13), // "getSweepValue"
QT_MOC_LITERAL(10, 112, 12), // "getVectorsUp"
QT_MOC_LITERAL(11, 125, 15), // "QVector<double>"
QT_MOC_LITERAL(12, 141, 10), // "getVectorX"
QT_MOC_LITERAL(13, 152, 10), // "getVectorY"
QT_MOC_LITERAL(14, 163, 14), // "getVectorsDown"
QT_MOC_LITERAL(15, 178, 6), // "getRaw"
QT_MOC_LITERAL(16, 185, 5), // "check"
QT_MOC_LITERAL(17, 191, 9), // "timerStop"
QT_MOC_LITERAL(18, 201, 10), // "timerStart"
QT_MOC_LITERAL(19, 212, 19), // "getRangeApplySignal"
QT_MOC_LITERAL(20, 232, 19), // "fromSpinBoxToXScale"
QT_MOC_LITERAL(21, 252, 19), // "fromSpinBoxToYScale"
QT_MOC_LITERAL(22, 272, 11), // "getNewRange"
QT_MOC_LITERAL(23, 284, 15), // "onXRangeChanged"
QT_MOC_LITERAL(24, 300, 15) // "onYRangeChanged"

    },
    "Graph\0rangeChangeToSpinBoxX\0\0QCPRange\0"
    "range\0rangeChangeToSpinBoxY\0updateGraph\0"
    "getFourier\0getSweep\0getSweepValue\0"
    "getVectorsUp\0QVector<double>\0getVectorX\0"
    "getVectorY\0getVectorsDown\0getRaw\0check\0"
    "timerStop\0timerStart\0getRangeApplySignal\0"
    "fromSpinBoxToXScale\0fromSpinBoxToYScale\0"
    "getNewRange\0onXRangeChanged\0onYRangeChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Graph[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       5,    1,   97,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  100,    2, 0x0a /* Public */,
       7,    1,  101,    2, 0x0a /* Public */,
       8,    1,  104,    2, 0x0a /* Public */,
      10,    2,  107,    2, 0x0a /* Public */,
      14,    2,  112,    2, 0x0a /* Public */,
      15,    1,  117,    2, 0x0a /* Public */,
      17,    0,  120,    2, 0x0a /* Public */,
      18,    0,  121,    2, 0x0a /* Public */,
      19,    0,  122,    2, 0x0a /* Public */,
      20,    1,  123,    2, 0x0a /* Public */,
      21,    1,  126,    2, 0x0a /* Public */,
      22,    0,  129,    2, 0x08 /* Private */,
      23,    1,  130,    2, 0x08 /* Private */,
      24,    1,  133,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::UShort,    9,
    QMetaType::Void, 0x80000000 | 11, 0x80000000 | 11,   12,   13,
    QMetaType::Void, 0x80000000 | 11, 0x80000000 | 11,   12,   13,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void Graph::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Graph *_t = static_cast<Graph *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->rangeChangeToSpinBoxX((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 1: _t->rangeChangeToSpinBoxY((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 2: _t->updateGraph(); break;
        case 3: _t->getFourier((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->getSweep((*reinterpret_cast< quint16(*)>(_a[1]))); break;
        case 5: _t->getVectorsUp((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 6: _t->getVectorsDown((*reinterpret_cast< QVector<double>(*)>(_a[1])),(*reinterpret_cast< QVector<double>(*)>(_a[2]))); break;
        case 7: _t->getRaw((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->timerStop(); break;
        case 9: _t->timerStart(); break;
        case 10: _t->getRangeApplySignal(); break;
        case 11: _t->fromSpinBoxToXScale((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 12: _t->fromSpinBoxToYScale((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 13: _t->getNewRange(); break;
        case 14: _t->onXRangeChanged((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        case 15: _t->onYRangeChanged((*reinterpret_cast< const QCPRange(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Graph::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::rangeChangeToSpinBoxX)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Graph::*_t)(const QCPRange & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Graph::rangeChangeToSpinBoxY)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject Graph::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Graph.data,
      qt_meta_data_Graph,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Graph::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Graph::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Graph.stringdata0))
        return static_cast<void*>(const_cast< Graph*>(this));
    return QWidget::qt_metacast(_clname);
}

int Graph::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void Graph::rangeChangeToSpinBoxX(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Graph::rangeChangeToSpinBoxY(const QCPRange & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
