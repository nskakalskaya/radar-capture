#include "mainwindow.h"
#include "testudp.h"
#include "fftw3.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), nextFrameNumber(0), defaultPortNumber(63), defaultFrameSize(514), defaultNumberOfFrames(8), defaultHarmonicValue(0.5),
      mask(0x0F)
{
    setMinimumWidth(900);
    setMinimumHeight(700);
    installEventFilter(this);
    MainGraph = new Graph(this);
    GUI = new Elements(this);
    Record = new Recorder(this);
    serverThread = new QThread;
    connect(this, &MainWindow::sendPortAddress, GUI, &Elements::setPortNumber);
    connect(this, &MainWindow::sendHarmonicNumber, GUI, &Elements::setHarmonicNumber);
    connect(this, &MainWindow::sendFrameSize, GUI, &Elements::setFrameSize);
    connect(this, &MainWindow::sendNumberOfPackets, GUI, &Elements::setNumberOfPackets);
    connect(this, &MainWindow::sendNewMax, GUI, &Elements::setNewMax);
    connect(GUI, &Elements::changeHarmonicValue, this, &MainWindow::newHarmonicValue);
    connect(GUI, &Elements::changeFrameSize, this, &MainWindow::newFrameSize);
    connect(GUI, &Elements::changeNumberFrames, this, &MainWindow::newNumberFrames);

    createMainLayout();
    setConfigFile();
    qRegisterMetaType <QByteArray > ("QByteArray&");
    Server = new TestUDP();
    Server->moveToThread(serverThread);
    connect(serverThread, &QThread::finished, Server, &QObject::deleteLater);
    connect(serverThread, &QThread::finished, serverThread, &QThread::deleteLater);
    connect(this, &MainWindow::startSocketSignal, Server, &TestUDP::startSocket, Qt::QueuedConnection);
//    connect(this, &MainWindow::startSocketSignal, Server, &TestUDP::startSocket, Qt::QueuedConnection);
//    connect(&serverThread, &QThread::started, this, [=] ()
//    {
//         emit startSocketSignal(portNumber);
//    });
    connect(GUI, &Elements::sendIsRecording, Record, &Recorder::getIsRecord);
    connect(Server, &TestUDP::sendFrame, this, &MainWindow::receiveFrame);
    connect(Record, &Recorder::sendSavedFrame, this, &MainWindow::receiveFrame);
    connect(Server, &TestUDP::sendFrame, Record, &Recorder::startRecording);
    connect(GUI, &Elements::sendPlaySignal, Server, &TestUDP::deleteSocket);
    connect(GUI, &Elements::sendPlaySignal, Record, &Recorder::startPlaying);
    connect(GUI, &Elements::sendRangeSignalX, MainGraph, &Graph::fromSpinBoxToXScale);
    connect(GUI, &Elements::sendRangeSignalY, MainGraph, &Graph::fromSpinBoxToYScale);
    connect(MainGraph, &Graph::rangeChangeToSpinBoxX, GUI, &Elements::getNewScaleToSpinX);
    connect(MainGraph, &Graph::rangeChangeToSpinBoxY, GUI, &Elements::getNewScaleToSpinY);
    connect(GUI, &Elements::sendFourierSignal, this, &MainWindow::getFourierValue);
    connect(GUI, &Elements::sendFourierSignal, MainGraph, &Graph::getFourier);
    connect(GUI, &Elements::sendRawSignal, this, &MainWindow::getRaw);
    connect(GUI, &Elements::sendRawSignal, MainGraph, &Graph::getRaw);
    connect(GUI, &Elements::sendEmbFourierSignal, this, &MainWindow::getEmbF);
    connect(this, &MainWindow::sendEmbFourier, MainGraph, &Graph::getEmbFourier);
    connect(MainGraph, &Graph::sendBorderValue, this, &MainWindow::getBorderValue);
    connect(MainGraph, &Graph::sendBorderValueInside, this, &MainWindow::getBorderValueInside);
    connect(this, &MainWindow::sendSweep, MainGraph, &Graph::getSweep);
    connect(this, &MainWindow::sendVectorsUp, MainGraph, &Graph::getVectorsUp);
    connect(this, &MainWindow::sendVectorsDown, MainGraph, &Graph::getVectorsDown);
    connect(GUI, &Elements::timerStops, MainGraph, &Graph::timerStop);
    connect(GUI, &Elements::timerStarts, MainGraph, &Graph::timerStart);
    connect(GUI, &Elements::applySignal, MainGraph, &Graph::getRangeApplySignal);
    serverThread->start();
    emit startSocketSignal(portNumber);
}

MainWindow::~MainWindow()
{
    serverThread->quit();
    serverThread->wait();
}

void MainWindow::createMainLayout()
{
    QVBoxLayout *mainVerticalLayout;
    mainVerticalLayout = new QVBoxLayout;
    mainVerticalLayout->addWidget(MainGraph,5);
    mainVerticalLayout->addWidget(GUI,0);
    QWidget *mainWidget = new QWidget(this);
    mainWidget->setLayout(mainVerticalLayout);
    setCentralWidget(mainWidget);
}

void MainWindow::setConfigFile()
{
    configFile = new QSettings("settings.ini", QSettings::IniFormat);
    QFileInfo fileChecked("settings.ini");
    if (!fileChecked.exists())
      createConfigFile();
     configFile->beginGroup("values");
     portNumber = configFile->value("port", 63).toInt();
     emit sendPortAddress(portNumber);
     harmonicNumber = configFile->value("harmonic", 0.5).toDouble();
     emit sendHarmonicNumber(harmonicNumber);
     sizeOfPacket = configFile->value("framesize", 514).toInt();
     emit sendFrameSize(sizeOfPacket);
     numberOfPackets = configFile->value("numberframes", 8).toInt();
     emit sendNumberOfPackets(numberOfPackets);
     configFile->endGroup();
     delete configFile;
}

void MainWindow::createConfigFile()
{
    configFile->remove("");
    configFile->beginGroup("values");
    configFile->setValue("port", defaultPortNumber);
    configFile->setValue("harmonic", defaultHarmonicValue);
    configFile->setValue("framesize", defaultFrameSize);
    configFile->setValue("numberframes", defaultNumberOfFrames);
    configFile->endGroup();
}

void MainWindow::receiveFrame(QByteArray Buffer)
{
    quint8 num, exponent, sweep;
    quint16 temp = 1;
    num = Buffer[0];
    sweep = num & mask;
    num = num >> 4;
    exponent = Buffer[1] - 243;
    short *dataShort = (short *)Buffer.data();
    dataShort = dataShort + 1;
    unsigned short *dataShortUnsigned = (unsigned short *)Buffer.data();
    dataShortUnsigned = dataShortUnsigned + 1;
    if (doRaw)
    {
        exponent = 1;
        ifFourier = false;
    }
    else if ((embFourier==true) || (doFourier == true))
         ifFourier = true;
    if (num == nextFrameNumber)
    {
       for (quint32 j=0; j<(sizeOfPacket/ 2) -1; j++)
       {
           if (ifFourier)
           {
               if (set.contains(sweep))
                    temp = 8;
               if (pairUp.contains(sweep))
                   ArrayFirstFourier.append((dataShortUnsigned[j]*temp) * 6 / exponent);
               else if (pairDown.contains(sweep))
                   ArraySecondFourier.append((dataShortUnsigned[j]*temp) *6 /exponent);
          }
          else
          {
               if (set.contains(sweep))
                    temp = 8;
               if (pairUp.contains(sweep))
                    ArrayFirst.append((dataShort[j]*temp) * 6 / exponent);
               else if (pairDown.contains(sweep))
                    ArraySecond.append((dataShort[j]*temp) *6 /exponent);
           }
       }
       nextFrameNumber++;
       if (num == numberOfPackets - 1)
       {
           if (pairUp.contains(sweep))
               {
                    if (ifFourier)
                    {
                        receiveSweepF(ArrayFirstFourier, sweep);
                        ArrayFirstFourier.clear();
                    }
                   else
                    {
                         receiveSweep(ArrayFirst, sweep);
                         ArrayFirst.clear();
                    }
               }

              else if (pairDown.contains(sweep))
               {
                  if (ifFourier)
                  {
                        receiveSweepF(ArraySecondFourier, sweep);
                        ArraySecondFourier.clear();
                  }
                  else
                  {
                       receiveSweep(ArraySecond, sweep);
                       ArraySecond.clear();
                  }
               }
           nextFrameNumber = 0;
       }
   }
   else
   {
       nextFrameNumber = 0;
       ArrayFirst.clear();
       ArraySecond.clear();
       ArrayFirstFourier.clear();
       ArraySecondFourier.clear();
   }
}

void MainWindow::receiveSweep(QVector<qint16> &sweep, quint16 sweepValue)
{
    gotSweep = sweepValue;
    emit sendSweep(gotSweep);
    if (pairUp.contains(sweepValue))
    {
        yUp.resize(sweep.size());
        xUp.resize(sweep.size());
        for (qint32 i = 0; i < sweep.size(); i++)
        {
             yUp[i]= static_cast<qreal>(sweep[i]);
             xUp[i]= static_cast<qreal>(i*harmonicNumber);
        }
        emit sendVectorsUp(xUp, yUp);
    }

    else if (pairDown.contains(sweepValue))
    {
        yDown.resize(sweep.size());
        xDown.resize(sweep.size());
        for (qint32 i=0; i<sweep.size(); i++)
        {
             yDown[i] = static_cast<qreal>(sweep[i]);
             xDown[i] = static_cast<qreal>(i*harmonicNumber);
        }
        emit sendVectorsDown(xDown, yDown);
    }
}

void MainWindow::receiveSweepF(QVector<quint16> &sweep, quint16 sweepValue)
{
    gotSweep = sweepValue;
    emit sendSweep(gotSweep);
    if (pairUp.contains(sweepValue))
    {
        yUp.resize(sweep.size());
        xUp.resize(sweep.size());
        for (qint32 i = 0; i < sweep.size(); i++)
        {
             yUp[i]= static_cast<qreal>(sweep[i]);
             xUp[i]= static_cast<qreal>(i*harmonicNumber);
        }
        emit sendVectorsUp(xUp, yUp);

    }

    else if (pairDown.contains(sweepValue))
    {
        yDown.resize(sweep.size());
        xDown.resize(sweep.size());
        for (int i=0; i<sweep.size(); i++)
        {
             yDown[i] = static_cast<qreal>(sweep[i]);
             xDown[i] = static_cast<qreal>(i*harmonicNumber);
        }
        emit sendVectorsDown(xDown, yDown);
    }
}

void MainWindow::newHarmonicValue(qreal harmonic)
{
    harmonicNumber = harmonic;
}

void MainWindow::newFrameSize(quint32 size)
{
    sizeOfPacket = size;
}

void MainWindow::newNumberFrames(quint32 number)
{
    numberOfPackets = number;
}

void MainWindow::getRaw(bool check)
{
    doRaw = check;
}

void MainWindow::getEmbF(bool check)
{
    embFourier = check;
    emit sendEmbFourier(check);
}

void MainWindow::getFourierValue(bool check)
{
    doFourier = check;
}

void MainWindow::getBorderValue(bool isUp, double getValue, QVector<double>fourierY, QVector<double> fourierX, QVector<double> reVec, QVector<double> imVec)
{
    borderValue = getValue;
    aboveBorder(isUp, fourierY, fourierX, reVec, imVec);
}

void MainWindow::getBorderValueInside(bool isUp, double getValue, QVector<double> yVector, QVector<double> xVector)
{
    borderValue = getValue;
    aboveBorderInside(isUp, yVector, xVector);
}

double MainWindow::aboveBorder(bool isUp, QVector<double> yVector, QVector<double> xVector, QVector<double>reVec, QVector<double>imVec)
{
    QVector<double>aboveY;
    QVector<double>aboveX;
    QVector<double>newSavedRe;
    QVector<double>newSavedIm;
    for (int i=0; i<yVector.size(); i++)
    {
        if (yVector[i] > borderValue)
        {
           aboveY.append(yVector[i]);
           aboveX.append(xVector[i]);
           newSavedRe.append(reVec.at(i));
           newSavedIm.append(imVec.at(i));
        }
    }
    QVector<double>::iterator it = aboveY.begin();
    std::advance(it, 1);
    double yMaxUp = *std::max_element(it, aboveY.end());
    int maxIndexX  = std::max_element(it, aboveY.end()) - it;
    double maxXValue = aboveX[maxIndexX];
    maxIndexX = maxIndexX + 1;
    QVector<double> outputThree, reThree, imThree;
    outputThree.append(aboveY.at(maxIndexX));
    outputThree.append(aboveY.at(maxIndexX-1));
    outputThree.append(aboveY.at(maxIndexX+1));
    reThree.append(newSavedRe.at(maxIndexX));
    reThree.append(newSavedRe.at(maxIndexX-1));
    reThree.append(newSavedRe.at(maxIndexX+1));
    imThree.append(newSavedIm.at(maxIndexX));
    imThree.append(newSavedIm.at(maxIndexX-1));
    imThree.append(newSavedIm.at(maxIndexX+1));
    double sigma;
    if (doFourier)
         sigma = reImJacobsen(reThree, imThree);
    emit sendNewMax(isUp, outputThree.at(0) + sigma);
}

double MainWindow::aboveBorderInside(bool isUp, QVector<double> yVector, QVector<double> xVector)
{
    QVector<double>arrayY;
    QVector<double>arrayX;

    for (int i=0; i<yVector.size(); i++)
    {
        if (yVector[i] > borderValue)
        {
           arrayY.append(yVector[i]);
           arrayX.append(xVector[i]);
        }
    }
    QVector<double>::iterator it = arrayY.begin();
    double yMaxUp = *std::max_element(it, arrayY.end());
    int maxIndexX  = std::max_element(it, arrayY.end()) - it;
    double maxXValue = arrayX[maxIndexX];
    QVector<double> outputThree;
    if (maxIndexX==0)
    {
        outputThree.append(arrayY.at(maxIndexX));
        outputThree.append(0);
        outputThree.append(arrayY.at(maxIndexX+1));
    }
    else if (maxIndexX == arrayY.size()-1)
    {
        outputThree.append(arrayY.at(maxIndexX));
        outputThree.append(arrayY.at(maxIndexX-1));
        outputThree.append(0);
    }
    else
    {
        outputThree.append(arrayY.at(maxIndexX));
        outputThree.append(arrayY.at(maxIndexX-1));
        outputThree.append(arrayY.at(maxIndexX+1));
    }

    double sigma;
    sigma = doJacobsen(outputThree);
    emit sendNewMax(isUp, outputThree.at(0) + sigma);
}

double MainWindow::doJacobsen(QVector<double> wholeVector)
{

    double sigma = (wholeVector.at(2)-wholeVector.at(1)) / (4*wholeVector.at(0)-2*wholeVector.at(1)-2*wholeVector.at(2));
    return sigma;
}


double MainWindow::reImJacobsen(QVector<double> reVec, QVector<double> imVec)
{
    double aR = reVec.at(2) - reVec.at(1);
    double aIm = imVec.at(2) - imVec.at(1);
    double bR = 2*reVec.at(0) - reVec.at(1) - reVec.at(2);
    double bIm = 2*imVec.at(0) - imVec.at(1) - imVec.at(2);
    double reSigma = ((aR*bR)+ (aIm*bIm)) / (bR*bR + bIm*bIm);
    reSigma *= -1;
    return reSigma;
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
       case Qt::Key_Escape:
           close();
           break;
       default:
           QMainWindow::keyPressEvent(event);
    }
}
