#include "testudp.h"
#include <QString>
TestUDP::TestUDP(QObject *parent) : QObject(parent)
{}

void TestUDP::readyRead()
{
   while (udpSocket->hasPendingDatagrams())
   {
        QByteArray Buffer;
        Buffer.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(Buffer.data(),Buffer.size());
        emit sendFrame(Buffer);

   }
}

void TestUDP::startSocket(quint32 portNumber)
{
   udpSocket = new QUdpSocket(this);
   udpSocket->bind(QHostAddress::Any, portNumber);
   //udpSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8 * 1024);
   connect(udpSocket, &QUdpSocket::readyRead, this, &TestUDP::readyRead);
}

void TestUDP::deleteSocket(bool isPlay, QString file)
{
    if (isPlay == true) {
         udpSocket->close();
         delete udpSocket;
    }
    else
    {
        startSocket(63);
    }
}
