#-------------------------------------------------
#
# Project created by QtCreator 2017-07-11T17:12:10
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = untitled
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    qcustomplot.cpp \
    testudp.cpp \
    graph.cpp \
    elements.cpp \
    recorder.cpp

HEADERS += \
        mainwindow.h \
    qcustomplot.h \
    testudp.h \
    graph.h \
    elements.h \
    recorder.h


INCLUDEPATH += C:\Users\n.skakalskaya\Desktop\RadarCapture\_1\fftw\include

LIBS += -LC:/Users/n.skakalskaya/Desktop/RadarCapture/_1/fftw/lib -llibfftw3-3 \
        -LC:/Users/n.skakalskaya/Desktop/RadarCapture/_1/fftw/lib -llibfftw3f-3 \
        -LC:/Users/n.skakalskaya/Desktop/RadarCapture/_1/fftw/lib -llibfftw3l-3

QMAKE_LIBDIR += C:/Users/n.skakalskaya/Desktop/RadarCapture/_1/fftw/lib



## ---------------- HOME ------------------------------
#INCLUDEPATH += C:\Users\asus\Desktop\mainTask\_files\fftw\include
#LIBS += -LC:/Users/asus/Desktop/mainTask/_files/fftw/lib -llibfftw3-3 \
#        -LC:/Users/asus/Desktop/mainTask/_files/fftw/lib -llibfftw3f-3 \
#        -LC:/Users/asus/Desktop/mainTask/_files/fftw/lib -llibfftw3l-3

#QMAKE_LIBDIR += C:/Users/asus/Desktop/mainTask/_files/fftw/lib

