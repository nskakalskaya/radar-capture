#ifndef GRAPH_H
#define GRAPH_H

#include <QWidget>
#include "qcustomplot.h"

#define XDEFAULTRANGE 1025
#define YDEFAULTRANGE 300

class Graph : public QWidget
{
    Q_OBJECT

public:
    explicit Graph(QWidget *parent);
    ~Graph();

    struct retFourier {
        QVector <double> transformVector;
        QVector <double> reVector;
        QVector <double> imVector;
 };

protected:

private:
    QCustomPlot *mainPlot;
    QTimer *timer;
    QVector <double> xUp, yUp, xDown, yDown, uF, dF, insY, insX;
    QVector <double> xArrayUp, yArrayUp, xArrayDown, yArrayDown, yUpInside, xUpInside, yDownInside, xDownInside;
    QSet <quint8> pairUp = {0xc, 0xe};
    QSet <quint8> pairDown = {0xf, 0xb};
    QSet <quint8> set = {0xe, 0xb};
    quint16 gotSweep;
    bool doFourier = false, doRaw = false, doEmb = false;
    double findNoise(QVector <double> xUp, QVector <double> yUp);
    QVector<double>findMaxMin(QVector<double> xVectorUp, QVector<double> xVectorDown, QVector<double> yVectorUp, QVector<double> yVectorDown);
    retFourier transformFourier(const QVector <double> &inputVector);

signals:
    void rangeChangeToSpinBoxX(const QCPRange &range);
    void rangeChangeToSpinBoxY(const QCPRange &range);
    void sendBorderValue(bool isUp, double sendValue, QVector <double>uf, QVector <double> xf, QVector <double> reVec, QVector<double> imVec);
    void sendBorderValueInside(bool isUp, double sendValue, QVector<double>sendY, QVector<double>sendX);

public slots:
   void updateGraph();
   void getFourier(bool getFourier);
   void getSweep(quint16 getSweepValue);
   void getVectorsUp(QVector <double> getVectorX, QVector <double> getVectorY);
   void getVectorsDown (QVector <double> getVectorX, QVector <double> getVectorY);
   void getRaw(bool check);
   void getEmbFourier(bool check);
   void timerStop();
   void timerStart();
   void getRangeApplySignal();
   void fromSpinBoxToXScale(const QCPRange &range);
   void fromSpinBoxToYScale(const QCPRange &range);

private slots:
   void getNewRange();
   void onXRangeChanged(const QCPRange &range);
   void onYRangeChanged(const QCPRange &range);
};

#endif // GRAPH_H
