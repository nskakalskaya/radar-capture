#ifndef RECORDER_H
#define RECORDER_H

#include <QWidget>
#include <QFile>


class Recorder : public QWidget
{
    Q_OBJECT
public:
    explicit Recorder(QWidget *parent = 0);

protected:

private:

    bool isRecording = false, isFileOpen = false;
    void stopRecording();
    int packets = 0;

signals:
   void sendSavedFrame(QByteArray &Buffer);

public slots:
    void getIsRecord(bool get);
    void startRecording(QByteArray &Buffer);
    void startPlaying(bool isPlay, QString fileName);

};

#endif // RECORDER_H
