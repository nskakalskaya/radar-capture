#include "elements.h"

Elements::Elements(QWidget *parent) : QWidget(parent)
{

    // Layouts
    QHBoxLayout *bottomHorizontalLayout = new QHBoxLayout;
    QVBoxLayout *elementsLayout = new QVBoxLayout;

    // Labels
    portNumberLabel = new QLabel("Port", this);
    harmonicValueLabel = new QLabel("Harmonic", this);
    frameSizeLabel = new QLabel("Frame size",this);
    framesNumberLabel = new QLabel("Number of frames", this);
    showNewMaxUpLabel = new QLabel("Estimation UP", this);
    showNewMaxDownLabel = new QLabel("Estimation DOWN", this);
    portNumberLabel->setFixedSize(100,30);
    harmonicValueLabel->setFixedSize(100,30);
    frameSizeLabel->setFixedSize(100,30);
    framesNumberLabel->setFixedSize(100,30);

    //Line Edits
    readPortLineEdit = new QLineEdit(this);
    readHNumberLineEdit = new QLineEdit(this);
    readSizeOfAFrameLineEdit = new QLineEdit(this);
    readNumberOfFramesLineEdit = new QLineEdit(this);
    showNewMaxUpLineEdit = new QLineEdit(this);
    showNewMaxDownLineEdit = new QLineEdit(this);
    readPortLineEdit->setFixedSize(80,20);
    readHNumberLineEdit->setFixedSize(80,20);
    readSizeOfAFrameLineEdit->setFixedSize(80,20);
    readNumberOfFramesLineEdit->setFixedSize(80,20);
    showNewMaxUpLineEdit->setFixedSize(80,20);
    showNewMaxDownLineEdit->setFixedSize(80,20);
    readPortLineEdit->setValidator(new QRegExpValidator(QRegExp( "[0-9]{1,5}")));
    readSizeOfAFrameLineEdit->setValidator(new QRegExpValidator(QRegExp( "[0-9]{1,4}")));
    readNumberOfFramesLineEdit->setValidator(new QRegExpValidator(QRegExp( "[0-9]{1,2}")));

    // Check Boxes
    rawDataCheckBox = new QCheckBox("Raw data", this);
    fourierCheckBox = new QCheckBox("Do Fourier",this);
    embeddedFourierCheckBox = new QCheckBox("Fourier inside",this);
    fourierCheckBox->setEnabled(false);
    connect(rawDataCheckBox, &QCheckBox::toggled, this, &Elements::doRawDataCheckBox);
    connect(fourierCheckBox, &QCheckBox::toggled, this, &Elements::doFourierCheckBox);
    connect(embeddedFourierCheckBox, &QCheckBox::toggled, this, &Elements::embeddedFourier);

    // Spin Boxes
    xScaleChangedSpinBox = new QDoubleSpinBox(this);
    xScaleChangedSpinBox2 = new QDoubleSpinBox(this);
    xScaleChangedSpinBox->setFixedSize(80,20);
    xScaleChangedSpinBox2->setFixedSize(80,20);
    xScaleChangedSpinBox->setMaximum(1000000);
    xScaleChangedSpinBox2->setMaximum(1000000);
    xScaleChangedSpinBox->setMinimum(-1000000);
    xScaleChangedSpinBox2->setMinimum(-1000000);
    xScaleChangedSpinBox->setValue(0);
    xScaleChangedSpinBox2->setValue(XDEFAULTRANGE);
    yScaleChangedSpinBox = new QDoubleSpinBox(this);
    yScaleChangedSpinBox2 = new QDoubleSpinBox(this);
    yScaleChangedSpinBox->setFixedSize(80,20);
    yScaleChangedSpinBox2->setFixedSize(80,20);
    yScaleChangedSpinBox->setMaximum(1000000);
    yScaleChangedSpinBox2->setMaximum(1000000);
    yScaleChangedSpinBox->setMinimum(-1000000);
    yScaleChangedSpinBox2->setMinimum(-1000000);
    yScaleChangedSpinBox->setValue(0);
    yScaleChangedSpinBox2->setValue(YDEFAULTRANGE);
    connect(xScaleChangedSpinBox,static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, [=]{
        QCPRange boundedRange;
        boundedRange.upper = xScaleChangedSpinBox->value();
        boundedRange.lower = xScaleChangedSpinBox2->value();
        emit sendRangeSignalX(boundedRange);
      });
    connect(xScaleChangedSpinBox2,static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, [=]{
        QCPRange boundedRange;
        boundedRange.upper = xScaleChangedSpinBox->value();
        boundedRange.lower = xScaleChangedSpinBox2->value();
        emit sendRangeSignalX(boundedRange);
    });
    connect(yScaleChangedSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, [=] {
        QCPRange boundedRange;
        boundedRange.upper = yScaleChangedSpinBox->value();
        boundedRange.lower = yScaleChangedSpinBox2->value();
        emit sendRangeSignalY(boundedRange);
    });
    connect(yScaleChangedSpinBox2, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, [=]{
        QCPRange boundedRange;
        boundedRange.upper = yScaleChangedSpinBox->value();
        boundedRange.lower = yScaleChangedSpinBox2->value();
        emit sendRangeSignalY(boundedRange);
    });

    // Buttons
    setDefaultRangeButton = new QPushButton("Default", this);
    saveRangeButton = new QPushButton("Save range", this);
    restoreRangeButton = new QPushButton("Restore", this);
    setDefaultRangeButton->setFixedSize(80,30);
    saveRangeButton->setFixedSize(80,30);
    restoreRangeButton->setFixedSize(80,30);
    connect(setDefaultRangeButton, &QPushButton::clicked, this, [=]{
        QCPRange boundedRangeX, boundedRangeY;
        boundedRangeX.lower = 0;
        boundedRangeX.upper = XDEFAULTRANGE;
        boundedRangeY.lower = 0;
        boundedRangeY.upper = YDEFAULTRANGE;
        emit sendRangeSignalX(boundedRangeX);
        emit sendRangeSignalY(boundedRangeY);
        xScaleChangedSpinBox->setValue(0);
        yScaleChangedSpinBox->setValue(0);
        xScaleChangedSpinBox2->setValue(XDEFAULTRANGE);
        yScaleChangedSpinBox2->setValue(YDEFAULTRANGE);
    });
    xAxisLabel = new QLabel(this);
    xAxisLabel->setFixedSize(80,20);
    xAxisLabel->setAlignment(Qt::AlignCenter);
    yAxisLabel = new QLabel(this);
    yAxisLabel->setFixedSize(80,20);
    yAxisLabel->setAlignment(Qt::AlignCenter);
    xAxisLabel->setText("xAxis");
    yAxisLabel->setText("yAxis");
    applyButton = new QPushButton("Apply", this);
    stopButton = new QPushButton("Stop", this);
    saveConfigButton = new QPushButton("Save config", this);
    saveConfigButton->setFixedHeight(50);
    saveConfigButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    stopButton->setFixedSize(40,50);
    applyButton->setFixedSize(40,50);
    applyButton->setEnabled(true);
    stopButton->setEnabled(true);
    saveConfigButton->setEnabled(true);
    connect(applyButton, &QPushButton::clicked, this, [=]
    {
        harmonicNumber = readHNumberLineEdit->text().toDouble();
        emit changeHarmonicValue(harmonicNumber);
        sizeOfPacket = readSizeOfAFrameLineEdit->text().toInt();
        emit changeFrameSize(sizeOfPacket);
        numberOfPackets = readNumberOfFramesLineEdit->text().toInt();
        emit changeNumberFrames(numberOfPackets);
        emit applySignal();

     });
    connect(stopButton, &QPushButton::clicked, this, [=]{
        if (isRunning)
        {
            applyButton->setEnabled(true);
            stopButton->setText("Start");
            emit timerStops();
            isRunning = false;
        }
        else
        {
            stopButton->setText("Stop");
            emit timerStarts();
            isRunning = true;
        }
    });
    connect(saveConfigButton, &QPushButton::clicked, this, [=]{

        QSettings settings("settings.ini", QSettings::IniFormat);
        settings.beginGroup("values");
        settings.setValue("port", readPortLineEdit->text());
        settings.setValue("harmonic",readHNumberLineEdit->text());
        settings.setValue("framesize", readSizeOfAFrameLineEdit->text());
        settings.setValue("numberframes",readNumberOfFramesLineEdit->text());
        settings.endGroup();
    });
    connect(saveRangeButton, &QPushButton::clicked, this, [=]
    {
        QSettings settings("settings.ini", QSettings::IniFormat);
        if (embFourier)
        {
           settings.beginGroup("values");
           settings.setValue("xupperEF", xScaleChangedSpinBox->value());
           settings.setValue("xlowerEF", xScaleChangedSpinBox2->value());
           settings.setValue("yupperEF", yScaleChangedSpinBox->value());
           settings.setValue("ylowerEF", yScaleChangedSpinBox2->value());
           settings.endGroup();
        }
        else if ((doRaw)&&(!doFourier))
        {
           settings.beginGroup("values");
           settings.setValue("xupperRaw", xScaleChangedSpinBox->value());
           settings.setValue("xlowerRaw", xScaleChangedSpinBox2->value());
           settings.setValue("yupperRaw", yScaleChangedSpinBox->value());
           settings.setValue("ylowerRaw", yScaleChangedSpinBox2->value());
           settings.endGroup();
        }
        else if (doFourier)
        {
               settings.beginGroup("values");
               settings.setValue("xupperF", xScaleChangedSpinBox->value());
               settings.setValue("xlowerF", xScaleChangedSpinBox2->value());
               settings.setValue("yupperF", yScaleChangedSpinBox->value());
               settings.setValue("ylowerF", yScaleChangedSpinBox2->value());
               settings.endGroup();
        }
    });
    connect(restoreRangeButton, &QPushButton::clicked, this, [=]{
        QSettings settings("settings.ini", QSettings::IniFormat);
        if (embFourier)
        {
            settings.beginGroup("values");
            xScaleChangedSpinBox->setValue(settings.value("xupperEF").toDouble());
            xScaleChangedSpinBox2->setValue(settings.value("xlowerEF").toDouble());
            yScaleChangedSpinBox->setValue(settings.value("yupperEF").toDouble());
            yScaleChangedSpinBox2->setValue(settings.value("ylowerEF").toDouble());
            settings.endGroup();
        }
        else if ((doRaw)&&(!doFourier))
        {
            settings.beginGroup("values");
            xScaleChangedSpinBox->setValue(settings.value("xupperRaw").toDouble());
            xScaleChangedSpinBox2->setValue(settings.value("xlowerRaw").toDouble());
            yScaleChangedSpinBox->setValue(settings.value("yupperRaw").toDouble());
            yScaleChangedSpinBox2->setValue(settings.value("ylowerRaw").toDouble());
            settings.endGroup();
        }
        else if (doFourier)
        {
            settings.beginGroup("values");
            xScaleChangedSpinBox->setValue(settings.value("xupperF").toDouble());
            xScaleChangedSpinBox2->setValue(settings.value("xlowerF").toDouble());
            yScaleChangedSpinBox->setValue(settings.value("yupperF").toDouble());
            yScaleChangedSpinBox2->setValue(settings.value("ylowerF").toDouble());
            settings.endGroup();
        }
    });
    startStopSavingButton = new QPushButton("Start", this);
    launchSavedButton = new QPushButton("Play", this);
    startStopSavingButton->setFixedSize(60,40);
    launchSavedButton->setFixedSize(60,40);
    startStopSavingButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    launchSavedButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    connect(startStopSavingButton, &QPushButton::clicked, this, [=]{
        if (isRecording) {
            startStopSavingButton->setText("Start");
            isRecording = false;
            emit sendIsRecording(isRecording);

        }
        else {
            startStopSavingButton->setText("Stop");
            isRecording = true;
            emit sendIsRecording(isRecording);
        }
    });
    connect(launchSavedButton, &QPushButton::clicked, this, [=]{
     if (isPlaying)
     {
         QString fileName;
         launchSavedButton->setText("Play");
         isPlaying = false;
         emit sendPlaySignal(isPlaying, fileName);
     }
     else
     {
         QString fileName = QFileDialog::getOpenFileName(this, tr("Choose log file"), "logs/");
         launchSavedButton->setText("Connect");
         isPlaying = true;
         emit sendPlaySignal(isPlaying, fileName);
     }

    });
    bottomHorizontalLayout->addStretch(1);
    bottomHorizontalLayout->addWidget(leftGroupBox(), 0, Qt::AlignCenter);
    bottomHorizontalLayout->addSpacing(10);
    bottomHorizontalLayout->addWidget(centerGroupBox(),0, Qt::AlignCenter);
    bottomHorizontalLayout->addSpacing(10);
    bottomHorizontalLayout->addWidget(rightGroupBox(),0, Qt::AlignCenter);
    bottomHorizontalLayout->addSpacing(10);
    bottomHorizontalLayout->addWidget(recordingGroupBox(),0, Qt::AlignCenter);
    bottomHorizontalLayout->addStretch(1);
    elementsLayout->addLayout(bottomHorizontalLayout);
    setLayout(elementsLayout);
}

Elements::~Elements()
{

}


QGroupBox *Elements::leftGroupBox()
{
    QGroupBox *groupBox = new QGroupBox(tr("Settings"));
    QVBoxLayout *common = new QVBoxLayout;
    QGridLayout *grid = new QGridLayout;
    QHBoxLayout *bottom = new QHBoxLayout;
    QHBoxLayout *veryBottom = new QHBoxLayout;
    grid->addWidget(readPortLineEdit,0,0);
    grid->addWidget(portNumberLabel,0,1);
    grid->addWidget(readHNumberLineEdit,1,0);
    grid->addWidget(harmonicValueLabel,1,1);
    grid->addWidget(readSizeOfAFrameLineEdit,0,2);
    grid->addWidget(frameSizeLabel,0,3);
    grid->addWidget(readNumberOfFramesLineEdit,1,2);
    grid->addWidget(framesNumberLabel,1,3);
    bottom->addStretch(1);
    bottom->addWidget(rawDataCheckBox);
    bottom->addStretch(1);
    bottom->addWidget(fourierCheckBox);
    bottom->addStretch(1);
    bottom->addWidget(embeddedFourierCheckBox);
    bottom->addStretch(1);
    veryBottom->addWidget(showNewMaxUpLineEdit);
    veryBottom->addWidget(showNewMaxUpLabel);
    veryBottom->addWidget(showNewMaxDownLineEdit);
    veryBottom->addWidget(showNewMaxDownLabel);
    common->addLayout(grid);
    common->addLayout(bottom, 0);
    common->addLayout(veryBottom, 0);
    groupBox->setLayout(common);
    groupBox->setFixedSize(370, 140);
    groupBox->setStyleSheet("QGroupBox { font-weight: bold; } ");
    return groupBox;
}

QGroupBox *Elements::rightGroupBox()
{
    QGroupBox *groupBox = new QGroupBox(tr("Range elements"));
    QGridLayout *gridRight = new QGridLayout;
    gridRight->addWidget(xAxisLabel, 0, 1);
    gridRight->addWidget(xScaleChangedSpinBox, 1, 1);
    gridRight->addWidget(xScaleChangedSpinBox2, 2, 1);
    gridRight->addWidget(yAxisLabel, 0, 2);
    gridRight->addWidget(yScaleChangedSpinBox, 1, 2);
    gridRight->addWidget(yScaleChangedSpinBox2, 2, 2);
    gridRight->addWidget(saveRangeButton, 0, 3);
    gridRight->addWidget(restoreRangeButton, 1, 3);
    gridRight->addWidget(setDefaultRangeButton, 2, 3);
    groupBox->setLayout(gridRight);
    groupBox->setFixedHeight(140);
    //groupBox->setFixedSize(370, 140);
    groupBox->setStyleSheet("QGroupBox { font-weight: bold; }");
    return groupBox;
}

QGroupBox *Elements::centerGroupBox()
{
    QGroupBox *groupBox = new QGroupBox(tr("Do changes"));
    QGridLayout *grid = new QGridLayout;
    grid->addWidget(applyButton, 0, 0);
    grid->addWidget(stopButton, 0, 1);
    grid->addWidget(saveConfigButton,1, 0, 1, 2);
    groupBox->setLayout(grid);
    groupBox->setFixedHeight(140);
    groupBox->setStyleSheet("QGroupBox { font-weight: bold; } ");
    return groupBox;
}

QGroupBox *Elements::recordingGroupBox()
{
    QGroupBox *groupBox = new QGroupBox(tr("Record"));
    QVBoxLayout *horizontal = new QVBoxLayout;
    horizontal->addWidget(startStopSavingButton);
    horizontal->addWidget(launchSavedButton);
    groupBox->setLayout(horizontal);
    groupBox->setFixedHeight(140);
    groupBox->setStyleSheet("QGroupBox { font-weight: bold; } ");
    return groupBox;
}


void Elements::doRawDataCheckBox(bool check)
{
    doRaw = check;
    emit sendRawSignal(doRaw);
    if (doRaw)
        fourierCheckBox->setEnabled(true);
    else
         fourierCheckBox->setEnabled(false);

}

void Elements::doFourierCheckBox(bool check)
{

    doFourier = check;
    emit sendFourierSignal(doFourier);
}

void Elements::embeddedFourier(bool check)
{
    embFourier = check;
    emit sendEmbFourierSignal(embFourier);
}

void Elements::setPortNumber(quint32 portNumber)
{
    readPortLineEdit->setText(QString::number(portNumber));
}

void Elements::setHarmonicNumber(qreal harmonic)
{
   readHNumberLineEdit->setText(QString::number(harmonic));
}

void Elements::setFrameSize(quint32 frameSize)
{
  readSizeOfAFrameLineEdit->setText(QString::number(frameSize));
}

void Elements::setNumberOfPackets(quint32 numberFrames)
{
    readNumberOfFramesLineEdit->setText(QString::number(numberFrames));
}

void Elements::setNewMax(bool isUp, double newMax)
{
    if (isUp)
        showNewMaxUpLineEdit->setText(QString::number(newMax));
    else
         showNewMaxDownLineEdit->setText(QString::number(newMax));
}

void Elements::getNewScaleToSpinX(const QCPRange &range)
{
    xScaleChangedSpinBox->setValue(range.upper);
    xScaleChangedSpinBox2->setValue(range.lower);
}

void Elements::getNewScaleToSpinY(const QCPRange &range)
{
    yScaleChangedSpinBox->setValue(range.upper);
    yScaleChangedSpinBox2->setValue(range.lower);
}


