#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QtCore>
#include "qcustomplot.h"
#include "testudp.h"
#include "graph.h"
#include "elements.h"
#include "recorder.h"

#define XDEFAULTRANGE 1025
#define YDEFAULTRANGE 300

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    TestUDP *Server;
    QThread *serverThread;
    Graph *MainGraph;
    Elements *GUI;
    Recorder *Record;
    QSettings *configFile;

    // --- Locals --- //
    // Containers
    QVector <qint16> ArrayFirst, ArraySecond;
    QVector <quint16> ArrayFirstFourier, ArraySecondFourier;
    QVector <double> xUp, yUp, xDown, yDown;
    QSet <quint8> pairUp = {0xc, 0xe};
    QSet <quint8> pairDown = {0xf, 0xb};
    QSet <quint8> set = {0xe, 0xb};
    // Variables
    quint32 nextFrameNumber, portNumber, sizeOfPacket, numberOfPackets, defaultPortNumber, defaultFrameSize, defaultNumberOfFrames;
    qreal harmonicNumber, defaultHarmonicValue;
    quint16 gotSweep;
    quint8 mask;
    double borderValue;
    bool doRaw = false, doFourier = false, isRunning = true, embFourier = false, ifFourier = false;
    void createMainLayout();
    void setConfigFile();
    void createConfigFile();
    double aboveBorder(bool isUp, QVector<double> yVector, QVector<double>xVector, QVector<double> reVec, QVector<double> imVec);
    double aboveBorderInside(bool isUp, QVector<double>yVector, QVector<double>xVector);
    double doJacobsen(QVector<double> wholeVector);
    double reImJacobsen(QVector<double> reVec, QVector<double> imVec);

signals:
    void sendPortAddress(quint32 portNumber);
    void sendHarmonicNumber(qreal harmonicNumber);
    void sendFrameSize(quint32 sizeOfPacket);
    void sendNumberOfPackets(quint32 numberOfPackets);
    void sendNewMax(bool isUp, double newMax);
    void startSocketSignal(quint32 portNumber);
    void sendSweep(quint16 gotSweep);
    void sendEmbFourier(bool check);
    void sendVectorsUp(QVector<double> xUp, QVector<double> yUp);
    void sendVectorsDown(QVector<double> xDown, QVector<double> yDown);

public slots:

private slots:
    void newHarmonicValue(qreal harmonic);
    void newFrameSize(quint32 size);
    void newNumberFrames(quint32 number);
    void receiveFrame(QByteArray Array);
    void receiveSweep(QVector <qint16> &sweep, quint16 sweepValue);
    void receiveSweepF(QVector <quint16> &sweep, quint16 sweepValue);
    void getRaw(bool check);
    void getEmbF(bool check);
    void getFourierValue(bool check);
    void getBorderValue(bool isUp, double getValue, QVector<double> fourierY, QVector<double> fourierX, QVector<double>reVec, QVector<double>imVec);
    void getBorderValueInside(bool isUp, double getValue, QVector<double> yVector, QVector<double> xVector);
};
#endif // MAINWINDOW_H
