#include "graph.h"
#include "fftw3.h"

Graph::Graph(QWidget *parent) : QWidget(parent)
{
    QPen pen;
    QHBoxLayout *middleHorizontalLayout = new QHBoxLayout;
    mainPlot = new QCustomPlot(this);
    mainPlot->setMinimumSize(700, 500);
    mainPlot->setEnabled(true);
    mainPlot->legend->setVisible(true);
    mainPlot->legend->setFont(QFont("Helvetica", 9));
    mainPlot->addGraph(0);
    mainPlot->graph(0)->setName("Upper sweep");
    pen.setColor(QColor(139, 70, 166));
    mainPlot->graph(0)->setPen(pen);
    mainPlot->graph(0)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsLine);
    mainPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot, 5));
    mainPlot->addGraph();
    mainPlot->graph(1)->setName("Lower sweep");
    pen.setColor(QColor(44, 168, 129));
    mainPlot->graph(1)->setPen(pen);
    mainPlot->graph(1)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsLine);
    mainPlot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot, 5));
    mainPlot->addGraph();
    pen.setColor(QColor(255,0,0));
    mainPlot->graph(2)->setName("Up sweep border");
    mainPlot->graph(2)->setPen(pen);
    mainPlot->addGraph();
    pen.setColor(QColor(0,255,0));
    mainPlot->graph(3)->setName("Down sweep border");
    mainPlot->graph(3)->setPen(pen);
    mainPlot->xAxis->setRange(0, XDEFAULTRANGE);
    mainPlot->yAxis->setRange(0, YDEFAULTRANGE);
    mainPlot->setInteraction(QCP::iRangeZoom, true);
    mainPlot->setInteraction(QCP::iRangeDrag, true);
    mainPlot->axisRect()->setRangeDrag(Qt::Horizontal | Qt::Vertical);
    mainPlot->axisRect()->setRangeZoom(Qt::Horizontal);
    connect(mainPlot->xAxis, SIGNAL(rangeChanged(QCPRange)),this,SLOT(onXRangeChanged(QCPRange)));
    connect(mainPlot->yAxis, SIGNAL(rangeChanged(QCPRange)),this,SLOT(onYRangeChanged(QCPRange)));
    middleHorizontalLayout->addWidget(mainPlot);
    setLayout(middleHorizontalLayout);  
    yArrayUp.resize(2);
    xArrayUp.resize(2);
    yArrayDown.resize(2);
    xArrayDown.resize(2);
    xUpInside.resize(2);
    yUpInside.resize(2);
    xDownInside.resize(2);
    yDownInside.resize(2);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Graph::updateGraph);
    timer->start(0);
}

Graph::~Graph()
{
}

double Graph::findNoise(QVector<double> xUp, QVector<double> yUp)
{
      qSort(yUp.begin(), yUp.end());
      QVector <double> sortedUp;
      quint16 length = yUp.size() / 10 + 1;
      quint16 part = length;
      length = yUp.size() - 2*length;
      sortedUp.resize(length);
      for (int i = 0; i<length; i++)
      {
          sortedUp[i] = yUp.at(i+part);
      }

      double sum = 0;
      for (int i = 0; i< sortedUp.size(); i++)
      {
          sum += sortedUp.at(i);
      }
      double noise = sum / sortedUp.size();
      double signalNoise = sortedUp.last() / noise;
      double result;
      if (signalNoise*0.35>9) {
          result =signalNoise*0.35*noise;
          return result;
      }
      else {
          result = 9*noise;
          return result;
      }
}

void Graph::updateGraph()
{
    if (pairUp.contains(gotSweep))
    {
        if (!doFourier) {
             mainPlot->graph(0)->setData(xUp, yUp);
             if (doEmb)
             {
                 yUpInside[0] = findNoise(xUp, yUp);
                 yUpInside[1] = yUpInside[0];
                 emit sendBorderValueInside(1, yUpInside[0], yUp, xUp);
                 xUpInside[0] = 0;
                 xUpInside[1] = 1024;
                 mainPlot->graph(2)->setData(xUpInside, yUpInside);
             }
        }
        else {
             retFourier retVal = transformFourier(yUp);
             mainPlot->graph(0)->setData(xUp, retVal.transformVector);
             uF.resize(yUp.size());
             uF = transformFourier(yUp).transformVector;
             yArrayUp[0] = findNoise(xUp, uF);
             yArrayUp[1] =  yArrayUp[0];
             emit sendBorderValue(1, yArrayUp[0], uF, xUp, retVal.reVector, retVal.imVector);
             xArrayUp[0] = 0;
             xArrayUp[1] = 1024;
             mainPlot->graph(2)->setData(xArrayUp, yArrayUp);

        }
        mainPlot->replot();
    }
    else if (pairDown.contains(gotSweep))
    {
        if (!doFourier) {
           mainPlot->graph(1)->setData(xDown, yDown);
           if (doEmb)
           {
               yDownInside[0] = findNoise(xDown, yDown);
               yDownInside[1] = yDownInside[0];
               emit sendBorderValueInside(0, yDownInside[0], yDown, xDown);
               xDownInside[0] = 0;
               xDownInside[1] = 1024;
               mainPlot->graph(3)->setData(xDownInside, yDownInside);
           }
        }
        else {
            retFourier retVal = transformFourier(yDown);
            mainPlot->graph(1)->setData(xDown, retVal.transformVector);
            dF.resize(yDown.size());
            dF = transformFourier(yDown).transformVector;
            yArrayDown[0] = findNoise(xUp, dF);
            yArrayDown[1] =  yArrayDown[0];
            emit sendBorderValue(0, yArrayDown[0], dF, xDown, retVal.reVector, retVal.imVector);
            xArrayDown[0] = 0;
            xArrayDown[1] = 1024;
            mainPlot->graph(3)->setData(xArrayDown, yArrayDown);

        }
        mainPlot->replot();
    }
}

void Graph::getFourier(bool getFourier)
{
    doFourier = getFourier;
    if (doFourier)
    {
        mainPlot->graph(0)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsImpulse);
        mainPlot->graph(1)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsImpulse);
    }
    else
    {
        mainPlot->graph(0)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsLine);
        mainPlot->graph(1)->setLineStyle((QCPGraph::LineStyle)QCPGraph::lsLine);
    }
}

void Graph::getSweep(quint16 getSweepValue)
{
    gotSweep = getSweepValue;

}

void Graph::getVectorsUp(QVector<double> getVectorX, QVector<double> getVectorY)
{
    xUp.resize(getVectorX.size());
    yUp.resize(getVectorX.size());
    xUp = getVectorX;
    yUp = getVectorY;

}

void Graph::getVectorsDown(QVector<double> getVectorX, QVector<double> getVectorY)
{
    xDown.resize(getVectorX.size());
    yDown.resize(getVectorX.size());
    xDown = getVectorX;
    yDown = getVectorY;
}

void Graph::getRaw(bool check)
{
    doRaw = check;
}

void Graph::getEmbFourier(bool check)
{
    doEmb = check;
}

void Graph::timerStop()
{
    timer->stop();
}

void Graph::timerStart()
{
    timer->start();
}

void Graph::getRangeApplySignal()
{
    QVector <double> newVec;
    if (doFourier)
        newVec = findMaxMin(xUp, xDown, uF, dF);
    else
        newVec = findMaxMin(xUp, xDown, yUp, yDown);
    mainPlot->xAxis->setRange(newVec.at(1), newVec.at(0));
    mainPlot->yAxis->setRange(newVec.at(3), newVec.at(2));
}

void Graph::getNewRange()
{
    QVector <double> newVec = findMaxMin(xUp, xDown, yUp, yDown);
    mainPlot->xAxis->setRange(newVec.at(1), newVec.at(0));
    mainPlot->yAxis->setRange(newVec.at(3), newVec.at(2));
}

void Graph::fromSpinBoxToXScale(const QCPRange &range)
{
    mainPlot->xAxis->setRange(range);
}

void Graph::fromSpinBoxToYScale(const QCPRange &range)
{
    mainPlot->yAxis->setRange(range);
}

void Graph::onXRangeChanged(const QCPRange &range)
{
    QCPRange currentRange = range;
    if (currentRange.lower < 0)
        currentRange.lower = 0;
    mainPlot->xAxis->setRange(currentRange);
    emit rangeChangeToSpinBoxX(currentRange);
}

void Graph::onYRangeChanged(const QCPRange &range)
{
    QCPRange currentRange = range;
    if (doFourier)
        currentRange.lower=0;
    mainPlot->yAxis->setRange(currentRange);


    emit rangeChangeToSpinBoxY(currentRange);
}


Graph::retFourier Graph::transformFourier(const QVector<double> &inputVector)
{
       int size = inputVector.size();
       QVector <double> outputVector, reVector, imVector;
       outputVector.resize(size / 2);
       fftw_plan plan;
       fftw_complex *cOut, *cIn;
       cOut = (fftw_complex*)fftw_malloc(size * sizeof(fftw_complex));
       cIn = (fftw_complex*)fftw_malloc(size * sizeof(fftw_complex));
       for (int i = 0; i < size; i++)
       {
           cIn[i][0] = inputVector[i];
           cIn[i][1] = 0;
       }
       plan = fftw_plan_dft_1d(size, cIn, cOut, FFTW_FORWARD, FFTW_ESTIMATE);
       fftw_execute(plan);
       reVector.clear();
       imVector.clear();
       for (int i = 0; i < size / 2; i++)
       {
           outputVector[i] = sqrt(cOut[i][0] * cOut[i][0] + cOut[i][1] * cOut[i][1]) / ((double)size);
           reVector.append(cOut[i][0]);
           imVector.append(cOut[i][1]);
       }
       fftw_destroy_plan(plan);
       fftw_free(cIn);
       fftw_free(cOut);
       return {outputVector, reVector, imVector};
}

QVector<double> Graph::findMaxMin(QVector<double> xVectorUp, QVector<double> xVectorDown, QVector<double> yVectorUp, QVector<double> yVectorDown)
{
    QVector <double> toReturn;
    double xMaxUp = *std::max_element(xVectorUp.begin(), xVectorUp.end());
    double xMaxDown = *std::max_element(xVectorDown.begin(), xVectorDown.end());
    double xMinUp = *std::min_element(xVectorUp.begin(), xVectorUp.end());
    double xMinDown = *std::min_element(xVectorDown.begin(), xVectorDown.end());
    double xMax = (xMaxUp >= xMaxDown ? xMaxUp : xMaxDown);
    double xMin = (xMinUp <= xMinDown ? xMinUp : xMinDown);
    toReturn.append(xMax);
    toReturn.append(xMin);
    double yMaxUp = *std::max_element(yVectorUp.begin(), yVectorUp.end());
    double yMaxDown = *std::max_element(yVectorDown.begin(), yVectorDown.end());
    double yMinUp = *std::min_element(yVectorUp.begin(), yVectorUp.end());
    double yMinDown = *std::min_element(yVectorDown.begin(), yVectorDown.end());
    double yMax = (yMaxUp >= yMaxDown ? yMaxUp : yMaxDown);
    double yMin = (yMinUp <= yMinDown ? yMinUp : yMinDown);
    toReturn.append(yMax);
    toReturn.append(yMin);
    return (toReturn);
}
