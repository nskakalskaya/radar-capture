#include "recorder.h"
#include <QDebug>
#include <QFile>
#include <QDataStream>
#include <QThread>
#include <QDateTime>

Recorder::Recorder(QWidget *parent) : QWidget(parent)
{

}

void Recorder::startRecording(QByteArray &Buffer)
{
    if (isRecording) {
        packets++;
        QDateTime now = QDateTime::currentDateTime();
        QString timestamp = now.toString(QLatin1String("hh.mm yyyy-MM-dd"));
        QString filename = QString::fromLatin1("logs/log_%1.dat").arg(timestamp);
        QFile file(filename);
        file.open(QIODevice::Append);
        QDataStream stream(&file);
        stream << Buffer;
        file.close();
    }
}

void Recorder::startPlaying(bool isPlay, QString fileName)
{
     QFile file(fileName);
     QByteArray Test;
     file.open(QIODevice::ReadOnly);
     QDataStream stream(&file);
     while (!file.atEnd()) {
            QByteArray Test;
            stream >> Test;
            //QThread::msleep(20);
            emit sendSavedFrame(Test);
      }
      file.close();
//     for (int j = 0; j < packets; j++)
//     {
//       char *tempExtra = new char[518];
//       char *tempNormal = new char[514];
//       stream.readRawData(tempExtra, 518);
//       for (int i = 0; i < 514; i++)
//       {
//           tempNormal[i] = tempExtra[i+4];
//       }
//       Test.clear();
//       Test.append(tempNormal, 514);
//       qDebug()<<Test.size() << "TESTSIZE";
//
//       emit sendSavedFrame(Test);
//       delete [] tempExtra;
//       delete [] tempNormal;
//     }
}

void Recorder::getIsRecord(bool get)
{
    isRecording = get;
}
