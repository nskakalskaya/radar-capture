#ifndef TESTUDP_H
#define TESTUDP_H
#include <QObject>
#include <QUdpSocket>
class TestUDP : public QObject
{
    Q_OBJECT

public:
    explicit TestUDP(QObject *parent = nullptr);
    void startSocket(quint32 portNumber);
    void processFrame(QByteArray &Buffer);


protected:

private:
    QUdpSocket *udpSocket;
    void readyRead();

signals:
      void sendFrame(QByteArray &buffer);

private slots:

public slots:
      void deleteSocket(bool isPlay, QString file);
};

#endif // TESTUDP_H
